# AMD Vulkan Bugs

This repository contains 2 bugs encountered with Mesa's radv Vulkan implementation when running the [OpenXR conformance tests](https://github.com/KhronosGroup/OpenXR-CTS) in [Monado](https://gitlab.freedesktop.org/monado/monado).
The code was extracted from the [OpenXR conformance tests](https://github.com/KhronosGroup/OpenXR-CTS/tree/devel/src/conformance).
Both bugs could not be reproduced on the Intel `iris` Mesa driver and on NVIDIA, while they can be reproduced on `amdvlk`.

### Dependencies

```
vulkan
shaderc
xcb
openxr
```

### Build

```
cmake . -G Ninja -B build
ninja -C build
```

### Validation

Both examples don't produce any Vulkan validation errors.
To check run with `VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation` set.

## hiz

This bug is reproducible without OpenXR.

To see bug:
```
./build/hiz
```

![hiz bad](screenshots/hiz-bad.png)

To work around bug and see cubes:
```
RADV_DEBUG=nohiz ./build/hiz
```
![hiz good](screenshots/hiz-good.png)

## fastclears

This bug is only reproducible with OpenXR running on the Monado runtime. Furthermore it can't be produced on a Navi 10 GPU, where it was visible on Vega 64 and integrated Vega.

The bug results in rendering corrupted memory.

To see bug:
```
./build/fastclears
```
![hiz bad](screenshots/fastclears-bad.png)

To work around bug:
```
RADV_DEBUG=nofastclears ./build/fastclears
```

![hiz bad](screenshots/fastclears-good.png)


