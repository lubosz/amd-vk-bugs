// Copyright (c) 2017-2020 The Khronos Group Inc.
// Copyright (c) 2020 Collabora Ltd.
// SPDX-License-Identifier: Apache-2.0

#include <assert.h>
#include <xcb/randr.h>
#include <xcb/xcb.h>

#include <algorithm>

#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>

#define USE_ONLINE_VULKAN_SHADERC
#include <shaderc/shaderc.hpp>

#define WIDTH 800
#define HEIGHT 600

#define THROW(message) printf(message);
#define VK_CHECK(result) assert(result == VK_SUCCESS)

#include <math.h>

struct XrMatrix4x4f {
  float m[16];
};

typedef struct XrFovf {
  float angleLeft;
  float angleRight;
  float angleUp;
  float angleDown;
} XrFovf;

typedef struct XrQuaternionf {
  float x;
  float y;
  float z;
  float w;
} XrQuaternionf;

typedef struct XrVector3f {
  float x;
  float y;
  float z;
} XrVector3f;

typedef struct XrPosef {
  XrQuaternionf orientation;
  XrVector3f position;
} XrPosef;

inline static void XrMatrix4x4f_CreateProjection(XrMatrix4x4f* result, const float tanAngleLeft,
                                                 const float tanAngleRight, const float tanAngleUp,
                                                 float const tanAngleDown, const float nearZ,
                                                 const float farZ) {
  const float tanAngleWidth = tanAngleRight - tanAngleLeft;
  const float tanAngleHeight = tanAngleDown - tanAngleUp;
  const float offsetZ = 0;

  result->m[0] = 2 / tanAngleWidth;
  result->m[4] = 0;
  result->m[8] = (tanAngleRight + tanAngleLeft) / tanAngleWidth;
  result->m[12] = 0;

  result->m[1] = 0;
  result->m[5] = 2 / tanAngleHeight;
  result->m[9] = (tanAngleUp + tanAngleDown) / tanAngleHeight;
  result->m[13] = 0;

  result->m[2] = 0;
  result->m[6] = 0;
  result->m[10] = -(farZ + offsetZ) / (farZ - nearZ);
  result->m[14] = -(farZ * (nearZ + offsetZ)) / (farZ - nearZ);

  result->m[3] = 0;
  result->m[7] = 0;
  result->m[11] = -1;
  result->m[15] = 0;
}

inline static void XrMatrix4x4f_CreateProjectionFov(XrMatrix4x4f* result, const XrFovf fov,
                                                    const float nearZ, const float farZ) {
  const float tanLeft = tanf(fov.angleLeft);
  const float tanRight = tanf(fov.angleRight);

  const float tanDown = tanf(fov.angleDown);
  const float tanUp = tanf(fov.angleUp);

  XrMatrix4x4f_CreateProjection(result, tanLeft, tanRight, tanUp, tanDown, nearZ, farZ);
}

inline static void XrMatrix4x4f_CreateScale(XrMatrix4x4f* result, const float x, const float y,
                                            const float z) {
  result->m[0] = x;
  result->m[1] = 0.0f;
  result->m[2] = 0.0f;
  result->m[3] = 0.0f;
  result->m[4] = 0.0f;
  result->m[5] = y;
  result->m[6] = 0.0f;
  result->m[7] = 0.0f;
  result->m[8] = 0.0f;
  result->m[9] = 0.0f;
  result->m[10] = z;
  result->m[11] = 0.0f;
  result->m[12] = 0.0f;
  result->m[13] = 0.0f;
  result->m[14] = 0.0f;
  result->m[15] = 1.0f;
}

inline static void XrMatrix4x4f_CreateFromQuaternion(XrMatrix4x4f* result,
                                                     const XrQuaternionf* quat) {
  const float x2 = quat->x + quat->x;
  const float y2 = quat->y + quat->y;
  const float z2 = quat->z + quat->z;

  const float xx2 = quat->x * x2;
  const float yy2 = quat->y * y2;
  const float zz2 = quat->z * z2;

  const float yz2 = quat->y * z2;
  const float wx2 = quat->w * x2;
  const float xy2 = quat->x * y2;
  const float wz2 = quat->w * z2;
  const float xz2 = quat->x * z2;
  const float wy2 = quat->w * y2;

  result->m[0] = 1.0f - yy2 - zz2;
  result->m[1] = xy2 + wz2;
  result->m[2] = xz2 - wy2;
  result->m[3] = 0.0f;

  result->m[4] = xy2 - wz2;
  result->m[5] = 1.0f - xx2 - zz2;
  result->m[6] = yz2 + wx2;
  result->m[7] = 0.0f;

  result->m[8] = xz2 + wy2;
  result->m[9] = yz2 - wx2;
  result->m[10] = 1.0f - xx2 - yy2;
  result->m[11] = 0.0f;

  result->m[12] = 0.0f;
  result->m[13] = 0.0f;
  result->m[14] = 0.0f;
  result->m[15] = 1.0f;
}

inline static void XrMatrix4x4f_CreateTranslation(XrMatrix4x4f* result, const float x,
                                                  const float y, const float z) {
  result->m[0] = 1.0f;
  result->m[1] = 0.0f;
  result->m[2] = 0.0f;
  result->m[3] = 0.0f;
  result->m[4] = 0.0f;
  result->m[5] = 1.0f;
  result->m[6] = 0.0f;
  result->m[7] = 0.0f;
  result->m[8] = 0.0f;
  result->m[9] = 0.0f;
  result->m[10] = 1.0f;
  result->m[11] = 0.0f;
  result->m[12] = x;
  result->m[13] = y;
  result->m[14] = z;
  result->m[15] = 1.0f;
}

inline static void XrMatrix4x4f_Multiply(XrMatrix4x4f* result, const XrMatrix4x4f* a,
                                         const XrMatrix4x4f* b) {
  result->m[0] = a->m[0] * b->m[0] + a->m[4] * b->m[1] + a->m[8] * b->m[2] + a->m[12] * b->m[3];
  result->m[1] = a->m[1] * b->m[0] + a->m[5] * b->m[1] + a->m[9] * b->m[2] + a->m[13] * b->m[3];
  result->m[2] = a->m[2] * b->m[0] + a->m[6] * b->m[1] + a->m[10] * b->m[2] + a->m[14] * b->m[3];
  result->m[3] = a->m[3] * b->m[0] + a->m[7] * b->m[1] + a->m[11] * b->m[2] + a->m[15] * b->m[3];

  result->m[4] = a->m[0] * b->m[4] + a->m[4] * b->m[5] + a->m[8] * b->m[6] + a->m[12] * b->m[7];
  result->m[5] = a->m[1] * b->m[4] + a->m[5] * b->m[5] + a->m[9] * b->m[6] + a->m[13] * b->m[7];
  result->m[6] = a->m[2] * b->m[4] + a->m[6] * b->m[5] + a->m[10] * b->m[6] + a->m[14] * b->m[7];
  result->m[7] = a->m[3] * b->m[4] + a->m[7] * b->m[5] + a->m[11] * b->m[6] + a->m[15] * b->m[7];

  result->m[8] = a->m[0] * b->m[8] + a->m[4] * b->m[9] + a->m[8] * b->m[10] + a->m[12] * b->m[11];
  result->m[9] = a->m[1] * b->m[8] + a->m[5] * b->m[9] + a->m[9] * b->m[10] + a->m[13] * b->m[11];
  result->m[10] = a->m[2] * b->m[8] + a->m[6] * b->m[9] + a->m[10] * b->m[10] + a->m[14] * b->m[11];
  result->m[11] = a->m[3] * b->m[8] + a->m[7] * b->m[9] + a->m[11] * b->m[10] + a->m[15] * b->m[11];

  result->m[12] =
      a->m[0] * b->m[12] + a->m[4] * b->m[13] + a->m[8] * b->m[14] + a->m[12] * b->m[15];
  result->m[13] =
      a->m[1] * b->m[12] + a->m[5] * b->m[13] + a->m[9] * b->m[14] + a->m[13] * b->m[15];
  result->m[14] =
      a->m[2] * b->m[12] + a->m[6] * b->m[13] + a->m[10] * b->m[14] + a->m[14] * b->m[15];
  result->m[15] =
      a->m[3] * b->m[12] + a->m[7] * b->m[13] + a->m[11] * b->m[14] + a->m[15] * b->m[15];
}

inline static void XrMatrix4x4f_CreateTranslationRotationScale(XrMatrix4x4f* result,
                                                               const XrVector3f* translation,
                                                               const XrQuaternionf* rotation,
                                                               const XrVector3f* scale) {
  XrMatrix4x4f scaleMatrix;
  XrMatrix4x4f_CreateScale(&scaleMatrix, scale->x, scale->y, scale->z);

  XrMatrix4x4f rotationMatrix;
  XrMatrix4x4f_CreateFromQuaternion(&rotationMatrix, rotation);

  XrMatrix4x4f translationMatrix;
  XrMatrix4x4f_CreateTranslation(&translationMatrix, translation->x, translation->y,
                                 translation->z);

  XrMatrix4x4f combinedMatrix;
  XrMatrix4x4f_Multiply(&combinedMatrix, &rotationMatrix, &scaleMatrix);
  XrMatrix4x4f_Multiply(result, &translationMatrix, &combinedMatrix);
}

inline static void XrMatrix4x4f_InvertRigidBody(XrMatrix4x4f* result, const XrMatrix4x4f* src) {
  result->m[0] = src->m[0];
  result->m[1] = src->m[4];
  result->m[2] = src->m[8];
  result->m[3] = 0.0f;
  result->m[4] = src->m[1];
  result->m[5] = src->m[5];
  result->m[6] = src->m[9];
  result->m[7] = 0.0f;
  result->m[8] = src->m[2];
  result->m[9] = src->m[6];
  result->m[10] = src->m[10];
  result->m[11] = 0.0f;
  result->m[12] = -(src->m[0] * src->m[12] + src->m[1] * src->m[13] + src->m[2] * src->m[14]);
  result->m[13] = -(src->m[4] * src->m[12] + src->m[5] * src->m[13] + src->m[6] * src->m[14]);
  result->m[14] = -(src->m[8] * src->m[12] + src->m[9] * src->m[13] + src->m[10] * src->m[14]);
  result->m[15] = 1.0f;
}

struct CmdBuffer {
  enum class CmdBufferState { Undefined, Initialized, Recording, Executable, Executing };

  CmdBufferState state{CmdBufferState::Undefined};
  VkCommandPool pool{VK_NULL_HANDLE};
  VkCommandBuffer buf{VK_NULL_HANDLE};
  VkFence execFence{VK_NULL_HANDLE};

  ~CmdBuffer();
  void Reset();
  bool Init(VkDevice device, uint32_t queueFamilyIndex);
  bool Begin();
  bool End();
  bool Exec(VkQueue queue);
  bool Wait();
  bool Clear();

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};

  void SetState(CmdBufferState newState) { state = newState; }
};

struct ShaderProgram {
  std::array<VkPipelineShaderStageCreateInfo, 2> shaderInfo{
      {{VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO},
       {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO}}};

  ShaderProgram() = default;

  void Reset();

  ~ShaderProgram() {}

  ShaderProgram(const ShaderProgram&) = delete;
  ShaderProgram& operator=(const ShaderProgram&) = delete;
  ShaderProgram(ShaderProgram&&) = delete;
  ShaderProgram& operator=(ShaderProgram&&) = delete;

  void LoadVertexShader(const std::vector<uint32_t>& code) { Load(0, code); }

  void LoadFragmentShader(const std::vector<uint32_t>& code) { Load(1, code); }

  void Init(VkDevice device) { m_vkDevice = device; }

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};

  void Load(uint32_t index, const std::vector<uint32_t>& code);
};

struct PipelineLayout {
  VkPipelineLayout layout{VK_NULL_HANDLE};

  PipelineLayout() = default;

  void Reset();

  ~PipelineLayout() { Reset(); }

  void Create(VkDevice device);

  PipelineLayout(const PipelineLayout&) = delete;
  PipelineLayout& operator=(const PipelineLayout&) = delete;
  PipelineLayout(PipelineLayout&&) = delete;
  PipelineLayout& operator=(PipelineLayout&&) = delete;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct RenderPass {
  VkFormat colorFmt{};
  VkFormat depthFmt{};
  VkRenderPass pass{VK_NULL_HANDLE};

  RenderPass() = default;

  bool Create(VkDevice device, VkFormat aColorFmt, VkFormat aDepthFmt);

  void Reset();

  ~RenderPass() { Reset(); }

  RenderPass(const RenderPass&) = delete;
  RenderPass& operator=(const RenderPass&) = delete;
  RenderPass(RenderPass&&) = delete;
  RenderPass& operator=(RenderPass&&) = delete;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct RenderTarget {
  VkImage colorImage{VK_NULL_HANDLE};
  VkImage depthImage{VK_NULL_HANDLE};
  VkImageView colorView{VK_NULL_HANDLE};
  VkImageView depthView{VK_NULL_HANDLE};
  VkFramebuffer fb{VK_NULL_HANDLE};

  RenderTarget() = default;

  ~RenderTarget();

  void Create(VkDevice device, VkImage aColorImage, VkImage aDepthImage, uint32_t baseArrayLayer,
              RenderPass& renderPass);

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct MemoryAllocator {
  void Init(VkPhysicalDevice physicalDevice, VkDevice device);

  void Reset();
  static const VkFlags defaultFlags =
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  void Allocate(VkMemoryRequirements const& memReqs, VkDeviceMemory* mem,
                VkFlags flags = defaultFlags, const void* pNext = nullptr) const;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  VkPhysicalDeviceMemoryProperties m_memProps{};
};

struct VertexBufferBase {
  VkBuffer idxBuf{VK_NULL_HANDLE};
  VkDeviceMemory idxMem{VK_NULL_HANDLE};
  VkBuffer vtxBuf{VK_NULL_HANDLE};
  VkDeviceMemory vtxMem{VK_NULL_HANDLE};
  VkVertexInputBindingDescription bindDesc{};
  std::vector<VkVertexInputAttributeDescription> attrDesc{};
  struct {
    uint32_t idx;
    uint32_t vtx;
  } count = {0, 0};

  VertexBufferBase() = default;

  void Reset();

  ~VertexBufferBase() { Reset(); }

  VertexBufferBase(const VertexBufferBase&) = delete;
  VertexBufferBase& operator=(const VertexBufferBase&) = delete;
  VertexBufferBase(VertexBufferBase&&) = delete;
  VertexBufferBase& operator=(VertexBufferBase&&) = delete;
  void Init(VkDevice device, const MemoryAllocator* memAllocator,
            const std::vector<VkVertexInputAttributeDescription>& attr);

 protected:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  void AllocateBufferMemory(VkBuffer buf, VkDeviceMemory* mem) const;

 private:
  const MemoryAllocator* m_memAllocator{nullptr};
};

template <typename T>
struct VertexBuffer : public VertexBufferBase {
  bool Create(uint32_t idxCount, uint32_t vtxCount) {
    VkBufferCreateInfo bufInfo{VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
    bufInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
    bufInfo.size = sizeof(uint16_t) * idxCount;
    VK_CHECK(vkCreateBuffer(m_vkDevice, &bufInfo, nullptr, &idxBuf));
    AllocateBufferMemory(idxBuf, &idxMem);
    VK_CHECK(vkBindBufferMemory(m_vkDevice, idxBuf, idxMem, 0));

    bufInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    bufInfo.size = sizeof(T) * vtxCount;
    VK_CHECK(vkCreateBuffer(m_vkDevice, &bufInfo, nullptr, &vtxBuf));
    AllocateBufferMemory(vtxBuf, &vtxMem);
    VK_CHECK(vkBindBufferMemory(m_vkDevice, vtxBuf, vtxMem, 0));

    bindDesc.binding = 0;
    bindDesc.stride = sizeof(T);
    bindDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    count = {idxCount, vtxCount};

    return true;
  }

  void UpdateIndicies(const uint16_t* data, uint32_t elements, uint32_t offset = 0) {
    uint16_t* map = nullptr;
    VK_CHECK(vkMapMemory(m_vkDevice, idxMem, sizeof(map[0]) * offset, sizeof(map[0]) * elements, 0,
                         (void**)&map));
    for (size_t i = 0; i < elements; ++i) {
      map[i] = data[i];
    }
    vkUnmapMemory(m_vkDevice, idxMem);
  }

  void UpdateVertices(const T* data, uint32_t elements, uint32_t offset = 0) {
    T* map = nullptr;
    VK_CHECK(vkMapMemory(m_vkDevice, vtxMem, sizeof(map[0]) * offset, sizeof(map[0]) * elements, 0,
                         (void**)&map));
    for (size_t i = 0; i < elements; ++i) {
      map[i] = data[i];
    }
    vkUnmapMemory(m_vkDevice, vtxMem);
  }
};

struct DepthBuffer {
  VkDeviceMemory depthMemory{VK_NULL_HANDLE};
  VkImage depthImage{VK_NULL_HANDLE};

  DepthBuffer() = default;
  ~DepthBuffer() { Reset(); }

  void Reset();

  void Create(VkDevice device, MemoryAllocator* memAllocator, VkFormat depthFormat);

  DepthBuffer(const DepthBuffer&) = delete;
  DepthBuffer& operator=(const DepthBuffer&) = delete;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct Pipeline {
  VkPipeline pipe{VK_NULL_HANDLE};
  VkPrimitiveTopology topology{VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST};
  std::vector<VkDynamicState> dynamicStateEnables;

  Pipeline() = default;
  ~Pipeline() { Reset(); }

  void Dynamic(VkDynamicState state) { dynamicStateEnables.emplace_back(state); }

  void Create(VkDevice device, const PipelineLayout& layout, const RenderPass& rp,
              const ShaderProgram& sp, const VertexBufferBase& vb);
  void Reset();

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct Vertex {
  XrVector3f Position;
  XrVector3f Color;
};

struct Swapchain {
  VkFormat format{VK_FORMAT_B8G8R8A8_SRGB};
  VkSurfaceKHR surface{VK_NULL_HANDLE};
  VkSwapchainKHR swapchain{VK_NULL_HANDLE};
  VkFence readyFence{VK_NULL_HANDLE};
  VkFence presentFence{VK_NULL_HANDLE};
  static const uint32_t maxImages = 4;
  uint32_t swapchainCount = 0;
  uint32_t renderImageIdx = 0;
  VkImage image[maxImages]{VK_NULL_HANDLE, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_NULL_HANDLE};

  Swapchain() {}
  ~Swapchain() { Reset(); }

  void Create(VkInstance instance, VkPhysicalDevice physDevice, VkDevice device,
              uint32_t queueFamilyIndex);
  void Prepare(VkCommandBuffer buf);
  void Wait();
  void Acquire(VkSemaphore readySemaphore = VK_NULL_HANDLE);
  void Present(VkQueue queue, VkSemaphore drawComplete = VK_NULL_HANDLE);
  void Reset() {
    if (m_vkDevice) {
      Wait();
      if (swapchain) vkDestroySwapchainKHR(m_vkDevice, swapchain, nullptr);
      if (readyFence) vkDestroyFence(m_vkDevice, readyFence, nullptr);
    }

    if (m_vkInstance && surface) vkDestroySurfaceKHR(m_vkInstance, surface, nullptr);

    readyFence = VK_NULL_HANDLE;
    presentFence = VK_NULL_HANDLE;
    swapchain = VK_NULL_HANDLE;
    surface = VK_NULL_HANDLE;
    for (uint32_t i = 0; i < swapchainCount; ++i) {
      image[i] = VK_NULL_HANDLE;
    }
    swapchainCount = 0;

    m_vkDevice = nullptr;
  }

 private:
  const VkExtent2D size{WIDTH, HEIGHT};
  VkInstance m_vkInstance{VK_NULL_HANDLE};
  VkPhysicalDevice m_vkPhysicalDevice{VK_NULL_HANDLE};
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  uint32_t m_queueFamilyIndex = 0;
};

struct Cube {
  XrPosef Pose;
  XrVector3f Scale;

  static inline Cube Make(XrVector3f position, float scale = 0.25f,
                          XrQuaternionf orientation = {0, 0, 0, 1}) {
    return Cube{/* pose */ {orientation, position},
                /* scale: */ {scale, scale, scale}};
  }
};

struct Example {
  Example(){};

  std::vector<const char*> ParseExtensionString(char* names) {
    std::vector<const char*> list;
    while (*names != 0) {
      list.push_back(names);
      while (*(++names) != 0) {
        if (*names == ' ') {
          *names++ = '\0';
          break;
        }
      }
    }
    return list;
  }

  bool InitializeDevice();

  std::vector<uint32_t> CompileGlslShader(const std::string& name, shaderc_shader_kind kind,
                                          const std::string& source);

  void InitializeResources();

  void ShutdownDevice();

  void SetViewportAndScissor(const VkRect2D& rect);

  void ClearImageSlice(uint32_t imageArrayIndex, int imageIndex);

  void RenderView(const std::vector<Cube>& cubes, int imageIndex);

  void BindRenderTarget(VkImage img, uint32_t arraySlice, const VkRect2D& renderArea,
                        VkRenderPassBeginInfo* renderPassBeginInfo, uint32_t index);

  void BindPipeline(VkCommandBuffer buf);

 private:
  VkInstance m_vkInstance{VK_NULL_HANDLE};
  VkPhysicalDevice m_vkPhysicalDevice{VK_NULL_HANDLE};
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  uint32_t m_queueFamilyIndex = 0;
  VkQueue m_vkQueue{VK_NULL_HANDLE};
  VkSemaphore m_vkDrawDone{VK_NULL_HANDLE};

  MemoryAllocator m_memAllocator{};
  ShaderProgram m_shaderProgram{};
  CmdBuffer m_cmdBuffer{};
  PipelineLayout m_pipelineLayout{};
  VertexBuffer<Vertex> m_drawBuffer{};

  std::vector<RenderTarget> renderTargets;
  DepthBuffer depthBuffer{};
  RenderPass rp{};
  Pipeline pipe{};

 public:
  Swapchain m_swapchain{};
};

constexpr XrVector3f Red{1, 0, 0};
constexpr XrVector3f DarkRed{0.25f, 0, 0};
constexpr XrVector3f Green{0, 1, 0};
constexpr XrVector3f DarkGreen{0, 0.25f, 0};
constexpr XrVector3f Blue{0, 0, 1};
constexpr XrVector3f DarkBlue{0, 0, 0.25f};

constexpr XrVector3f LBB{-0.5f, -0.5f, -0.5f};
constexpr XrVector3f LBF{-0.5f, -0.5f, 0.5f};
constexpr XrVector3f LTB{-0.5f, 0.5f, -0.5f};
constexpr XrVector3f LTF{-0.5f, 0.5f, 0.5f};
constexpr XrVector3f RBB{0.5f, -0.5f, -0.5f};
constexpr XrVector3f RBF{0.5f, -0.5f, 0.5f};
constexpr XrVector3f RTB{0.5f, 0.5f, -0.5f};
constexpr XrVector3f RTF{0.5f, 0.5f, 0.5f};

#define CUBE_SIDE(V1, V2, V3, V4, V5, V6, COLOR) \
  {V1, COLOR}, {V2, COLOR}, {V3, COLOR}, {V4, COLOR}, {V5, COLOR}, {V6, COLOR},

constexpr std::array<Vertex, 36> c_cubeVertices{{
    CUBE_SIDE(LTB, LBF, LBB, LTB, LTF, LBF, DarkRed)    // -X
    CUBE_SIDE(RTB, RBB, RBF, RTB, RBF, RTF, Red)        // +X
    CUBE_SIDE(LBB, LBF, RBF, LBB, RBF, RBB, DarkGreen)  // -Y
    CUBE_SIDE(LTB, RTB, RTF, LTB, RTF, LTF, Green)      // +Y
    CUBE_SIDE(LBB, RBB, RTB, LBB, RTB, LTB, DarkBlue)   // -Z
    CUBE_SIDE(LBF, LTF, RTF, LBF, RTF, RBF, Blue)       // +Z
}};

constexpr std::array<unsigned short, 36> c_cubeIndices{{
    0,  1,  2,  3,  4,  5,   // -X
    6,  7,  8,  9,  10, 11,  // +X
    12, 13, 14, 15, 16, 17,  // -Y
    18, 19, 20, 21, 22, 23,  // +Y
    24, 25, 26, 27, 28, 29,  // -Z
    30, 31, 32, 33, 34, 35,  // +Z
}};

constexpr char VertexShaderGlsl[] =
    R"_(
	    #version 400
	    #extension GL_ARB_separate_shader_objects : enable
	    #extension GL_ARB_shading_language_420pack : enable

	    #pragma vertex

	    layout (std140, push_constant) uniform buf
	    {
	        mat4 mvp;
	    } ubuf;

	    layout (location = 0) in vec3 Position;
	    layout (location = 1) in vec3 Color;

	    layout (location = 0) out vec4 oColor;
	    out gl_PerVertex
	    {
	        vec4 gl_Position;
	    };

	    void main()
	    {
	        oColor.rgb  = Color.rgb;
	        oColor.a  = 1.0;
	        gl_Position = ubuf.mvp * vec4(Position, 1);
	    }
)_";

constexpr char FragmentShaderGlsl[] =
    R"_(
	    #version 400
	    #extension GL_ARB_separate_shader_objects : enable
	    #extension GL_ARB_shading_language_420pack : enable

	    #pragma fragment

	    layout (location = 0) in vec4 oColor;

	    layout (location = 0) out vec4 FragColor;

	    void main()
	    {
	        FragColor = oColor;
	    }
)_";

void MemoryAllocator::Init(VkPhysicalDevice physicalDevice, VkDevice device) {
  m_vkDevice = device;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &m_memProps);
}

void MemoryAllocator::Reset() {
  m_memProps = {};
  m_vkDevice = VK_NULL_HANDLE;
}

void MemoryAllocator::Allocate(VkMemoryRequirements const& memReqs, VkDeviceMemory* mem,
                               VkFlags flags, const void* pNext) const {
  // Search memtypes to find first index with those properties
  for (uint32_t i = 0; i < m_memProps.memoryTypeCount; ++i) {
    if ((memReqs.memoryTypeBits & (1 << i)) != 0u) {
      // Type is available, does it match user properties?
      if ((m_memProps.memoryTypes[i].propertyFlags & flags) == flags) {
        VkMemoryAllocateInfo memAlloc{VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, pNext};
        memAlloc.allocationSize = memReqs.size;
        memAlloc.memoryTypeIndex = i;
        VK_CHECK(vkAllocateMemory(m_vkDevice, &memAlloc, nullptr, mem));
        return;
      }
    }
  }
  THROW("Memory format not supported");
}

void CmdBuffer::Reset() {
  SetState(CmdBufferState::Undefined);
  if (m_vkDevice != nullptr) {
    if (buf != VK_NULL_HANDLE) {
      vkFreeCommandBuffers(m_vkDevice, pool, 1, &buf);
    }
    if (pool != VK_NULL_HANDLE) {
      vkDestroyCommandPool(m_vkDevice, pool, nullptr);
    }
    if (execFence != VK_NULL_HANDLE) {
      vkDestroyFence(m_vkDevice, execFence, nullptr);
    }
  }
  buf = VK_NULL_HANDLE;
  pool = VK_NULL_HANDLE;
  execFence = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

CmdBuffer::~CmdBuffer() { Reset(); }

bool CmdBuffer::Init(VkDevice device, uint32_t queueFamilyIndex) {
  assert((state == CmdBufferState::Undefined) || (state == CmdBufferState::Initialized));

  m_vkDevice = device;

  VkCommandPoolCreateInfo cmdPoolInfo{VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
  cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  cmdPoolInfo.queueFamilyIndex = queueFamilyIndex;
  VK_CHECK(vkCreateCommandPool(m_vkDevice, &cmdPoolInfo, nullptr, &pool));

  VkCommandBufferAllocateInfo cmd{VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
  cmd.commandPool = pool;
  cmd.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  cmd.commandBufferCount = 1;
  VK_CHECK(vkAllocateCommandBuffers(m_vkDevice, &cmd, &buf));

  VkFenceCreateInfo fenceInfo{VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  VK_CHECK(vkCreateFence(m_vkDevice, &fenceInfo, nullptr, &execFence));

  SetState(CmdBufferState::Initialized);
  return true;
}

bool CmdBuffer::Begin() {
  assert(state == CmdBufferState::Initialized);
  VkCommandBufferBeginInfo cmdBeginInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
  VK_CHECK(vkBeginCommandBuffer(buf, &cmdBeginInfo));
  SetState(CmdBufferState::Recording);
  return true;
}

bool CmdBuffer::End() {
  assert(state == CmdBufferState::Recording);
  VK_CHECK(vkEndCommandBuffer(buf));
  SetState(CmdBufferState::Executable);
  return true;
}

bool CmdBuffer::Exec(VkQueue queue) {
  assert(state == CmdBufferState::Executable);

  VkSubmitInfo submitInfo{VK_STRUCTURE_TYPE_SUBMIT_INFO};
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &buf;
  VK_CHECK(vkQueueSubmit(queue, 1, &submitInfo, execFence));

  SetState(CmdBufferState::Executing);
  return true;
}

bool CmdBuffer::Wait() {
  if (state == CmdBufferState::Initialized) {
    return true;
  }

  assert(state == CmdBufferState::Executing);

  const uint32_t timeoutNs = 1 * 1000 * 1000 * 1000;
  for (int i = 0; i < 5; ++i) {
    auto res = vkWaitForFences(m_vkDevice, 1, &execFence, VK_TRUE, timeoutNs);
    if (res == VK_SUCCESS) {
      SetState(CmdBufferState::Executable);
      return true;
    }
  }

  return false;
}

bool CmdBuffer::Clear() {
  if (state != CmdBufferState::Initialized) {
    assert(state == CmdBufferState::Executable);

    VK_CHECK(vkResetFences(m_vkDevice, 1, &execFence));
    VK_CHECK(vkResetCommandBuffer(buf, 0));

    SetState(CmdBufferState::Initialized);
  }

  return true;
}

void ShaderProgram::Reset() {
  if (m_vkDevice != nullptr) {
    for (auto& si : shaderInfo) {
      if (si.module != VK_NULL_HANDLE) {
        vkDestroyShaderModule(m_vkDevice, si.module, nullptr);
      }
      si.module = VK_NULL_HANDLE;
    }
  }
  shaderInfo = {};
  m_vkDevice = nullptr;
}

void ShaderProgram::Load(uint32_t index, const std::vector<uint32_t>& code) {
  VkShaderModuleCreateInfo modInfo{VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO};

  auto& si = shaderInfo[index];
  si.pName = "main";
  std::string name;

  switch (index) {
    case 0:
      si.stage = VK_SHADER_STAGE_VERTEX_BIT;
      name = "vertex";
      break;
    case 1:
      si.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
      name = "fragment";
      break;
    default:
      THROW("Unknown code index");
  }

  modInfo.codeSize = code.size() * sizeof(code[0]);
  modInfo.pCode = &code[0];

  VK_CHECK(vkCreateShaderModule(m_vkDevice, &modInfo, nullptr, &si.module));
}

void VertexBufferBase::Reset() {
  if (m_vkDevice != nullptr) {
    if (idxBuf != VK_NULL_HANDLE) {
      vkDestroyBuffer(m_vkDevice, idxBuf, nullptr);
    }
    if (idxMem != VK_NULL_HANDLE) {
      vkFreeMemory(m_vkDevice, idxMem, nullptr);
    }
    if (vtxBuf != VK_NULL_HANDLE) {
      vkDestroyBuffer(m_vkDevice, vtxBuf, nullptr);
    }
    if (vtxMem != VK_NULL_HANDLE) {
      vkFreeMemory(m_vkDevice, vtxMem, nullptr);
    }
  }
  idxBuf = VK_NULL_HANDLE;
  idxMem = VK_NULL_HANDLE;
  vtxBuf = VK_NULL_HANDLE;
  vtxMem = VK_NULL_HANDLE;
  bindDesc = {};
  attrDesc.clear();
  count = {0, 0};
  m_vkDevice = nullptr;
}

void VertexBufferBase::Init(VkDevice device, const MemoryAllocator* memAllocator,
                            const std::vector<VkVertexInputAttributeDescription>& attr) {
  m_vkDevice = device;
  m_memAllocator = memAllocator;
  attrDesc = attr;
}

void VertexBufferBase::AllocateBufferMemory(VkBuffer buf, VkDeviceMemory* mem) const {
  VkMemoryRequirements memReq = {};
  vkGetBufferMemoryRequirements(m_vkDevice, buf, &memReq);
  m_memAllocator->Allocate(memReq, mem);
}

bool RenderPass::Create(VkDevice device, VkFormat aColorFmt, VkFormat aDepthFmt) {
  m_vkDevice = device;
  colorFmt = aColorFmt;
  depthFmt = aDepthFmt;

  VkSubpassDescription subpass = {};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

  VkAttachmentReference colorRef = {0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
  VkAttachmentReference depthRef = {1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};

  std::array<VkAttachmentDescription, 2> at = {};

  VkRenderPassCreateInfo rpInfo{VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
  rpInfo.attachmentCount = 0;
  rpInfo.pAttachments = at.data();
  rpInfo.subpassCount = 1;
  rpInfo.pSubpasses = &subpass;

  if (colorFmt != VK_FORMAT_UNDEFINED) {
    colorRef.attachment = rpInfo.attachmentCount++;

    at[colorRef.attachment].format = colorFmt;
    at[colorRef.attachment].samples = VK_SAMPLE_COUNT_1_BIT;
    at[colorRef.attachment].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    at[colorRef.attachment].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    at[colorRef.attachment].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    at[colorRef.attachment].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    at[colorRef.attachment].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    at[colorRef.attachment].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorRef;
  }

  if (depthFmt != VK_FORMAT_UNDEFINED) {
    depthRef.attachment = rpInfo.attachmentCount++;

    at[depthRef.attachment].format = depthFmt;
    at[depthRef.attachment].samples = VK_SAMPLE_COUNT_1_BIT;
    at[depthRef.attachment].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    at[depthRef.attachment].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    at[depthRef.attachment].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    at[depthRef.attachment].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    at[depthRef.attachment].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    at[depthRef.attachment].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    subpass.pDepthStencilAttachment = &depthRef;
  }

  VK_CHECK(vkCreateRenderPass(m_vkDevice, &rpInfo, nullptr, &pass));

  return true;
}

void RenderPass::Reset() {
  if (m_vkDevice != nullptr) {
    if (pass != VK_NULL_HANDLE) {
      vkDestroyRenderPass(m_vkDevice, pass, nullptr);
    }
  }
  pass = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

RenderTarget::~RenderTarget() {
  if (m_vkDevice != VK_NULL_HANDLE) {
    if (fb != VK_NULL_HANDLE) {
      vkDestroyFramebuffer(m_vkDevice, fb, nullptr);
    }
    if (colorView != VK_NULL_HANDLE) {
      vkDestroyImageView(m_vkDevice, colorView, nullptr);
    }
    if (depthView != VK_NULL_HANDLE) {
      vkDestroyImageView(m_vkDevice, depthView, nullptr);
    }
  }

  colorImage = VK_NULL_HANDLE;
  depthImage = VK_NULL_HANDLE;
  colorView = VK_NULL_HANDLE;
  depthView = VK_NULL_HANDLE;
  fb = VK_NULL_HANDLE;
  m_vkDevice = VK_NULL_HANDLE;
}

void RenderTarget::Create(VkDevice device, VkImage aColorImage, VkImage aDepthImage,
                          uint32_t baseArrayLayer, RenderPass& renderPass) {
  m_vkDevice = device;

  colorImage = aColorImage;
  depthImage = aDepthImage;

  std::array<VkImageView, 2> attachments{};
  uint32_t attachmentCount = 0;

  if (colorImage != VK_NULL_HANDLE) {
    VkImageViewCreateInfo colorViewInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    colorViewInfo.image = colorImage;
    colorViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    colorViewInfo.format = renderPass.colorFmt;
    colorViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    colorViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    colorViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    colorViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    colorViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    colorViewInfo.subresourceRange.baseMipLevel = 0;
    colorViewInfo.subresourceRange.levelCount = 1;
    colorViewInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
    colorViewInfo.subresourceRange.layerCount = 1;
    VK_CHECK(vkCreateImageView(m_vkDevice, &colorViewInfo, nullptr, &colorView));
    attachments[attachmentCount++] = colorView;
  }

  if (depthImage != VK_NULL_HANDLE) {
    VkImageViewCreateInfo depthViewInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    depthViewInfo.image = depthImage;
    depthViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    depthViewInfo.format = renderPass.depthFmt;
    depthViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    depthViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    depthViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    depthViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    depthViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    depthViewInfo.subresourceRange.baseMipLevel = 0;
    depthViewInfo.subresourceRange.levelCount = 1;
    depthViewInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
    depthViewInfo.subresourceRange.layerCount = 1;
    VK_CHECK(vkCreateImageView(m_vkDevice, &depthViewInfo, nullptr, &depthView));
    attachments[attachmentCount++] = depthView;
  }

  VkFramebufferCreateInfo fbInfo{VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
  fbInfo.renderPass = renderPass.pass;
  fbInfo.attachmentCount = attachmentCount;
  fbInfo.pAttachments = attachments.data();
  fbInfo.width = WIDTH;
  fbInfo.height = HEIGHT;
  fbInfo.layers = 1;
  VK_CHECK(vkCreateFramebuffer(m_vkDevice, &fbInfo, nullptr, &fb));
}

void PipelineLayout::Reset() {
  if (m_vkDevice != nullptr) {
    if (layout != VK_NULL_HANDLE) {
      vkDestroyPipelineLayout(m_vkDevice, layout, nullptr);
    }
  }
  layout = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

void PipelineLayout::Create(VkDevice device) {
  m_vkDevice = device;

  VkPushConstantRange pcr = {};
  pcr.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
  pcr.offset = 0;
  pcr.size = 4 * 4 * sizeof(float);

  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{
      VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
  pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
  pipelineLayoutCreateInfo.pPushConstantRanges = &pcr;
  VK_CHECK(vkCreatePipelineLayout(m_vkDevice, &pipelineLayoutCreateInfo, nullptr, &layout));
}

void Pipeline::Create(VkDevice device, const PipelineLayout& layout, const RenderPass& rp,
                      const ShaderProgram& sp, const VertexBufferBase& vb) {
  m_vkDevice = device;

  VkPipelineDynamicStateCreateInfo dynamicState{
      VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO};
  dynamicState.dynamicStateCount = (uint32_t)dynamicStateEnables.size();
  dynamicState.pDynamicStates = dynamicStateEnables.data();

  VkPipelineVertexInputStateCreateInfo vi{
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};
  vi.vertexBindingDescriptionCount = 1;
  vi.pVertexBindingDescriptions = &vb.bindDesc;
  vi.vertexAttributeDescriptionCount = (uint32_t)vb.attrDesc.size();
  vi.pVertexAttributeDescriptions = vb.attrDesc.data();

  VkPipelineInputAssemblyStateCreateInfo ia{
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO};
  ia.primitiveRestartEnable = VK_FALSE;
  ia.topology = topology;

  VkPipelineRasterizationStateCreateInfo rs{
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO};
  rs.polygonMode = VK_POLYGON_MODE_FILL;
  rs.cullMode = VK_CULL_MODE_BACK_BIT;
  rs.frontFace = VK_FRONT_FACE_CLOCKWISE;
  rs.depthClampEnable = VK_FALSE;
  rs.rasterizerDiscardEnable = VK_FALSE;
  rs.depthBiasEnable = VK_FALSE;
  rs.depthBiasConstantFactor = 0;
  rs.depthBiasClamp = 0;
  rs.depthBiasSlopeFactor = 0;
  rs.lineWidth = 1.0f;

  VkPipelineColorBlendAttachmentState attachState{};
  attachState.blendEnable = 0;
  attachState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
  attachState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
  attachState.colorBlendOp = VK_BLEND_OP_ADD;
  attachState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  attachState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  attachState.alphaBlendOp = VK_BLEND_OP_ADD;
  attachState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                               VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

  VkPipelineColorBlendStateCreateInfo cb{VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO};
  cb.attachmentCount = 1;
  cb.pAttachments = &attachState;
  cb.logicOpEnable = VK_FALSE;
  cb.logicOp = VK_LOGIC_OP_NO_OP;
  cb.blendConstants[0] = 1.0f;
  cb.blendConstants[1] = 1.0f;
  cb.blendConstants[2] = 1.0f;
  cb.blendConstants[3] = 1.0f;

  VkPipelineViewportStateCreateInfo vp{VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO};
  vp.viewportCount = 1;
  vp.scissorCount = 1;

  VkPipelineDepthStencilStateCreateInfo ds{
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO};
  ds.depthTestEnable = VK_TRUE;
  ds.depthWriteEnable = VK_TRUE;
  ds.depthCompareOp = VK_COMPARE_OP_LESS;
  ds.depthBoundsTestEnable = VK_FALSE;
  ds.stencilTestEnable = VK_FALSE;
  ds.front.failOp = VK_STENCIL_OP_KEEP;
  ds.front.passOp = VK_STENCIL_OP_KEEP;
  ds.front.depthFailOp = VK_STENCIL_OP_KEEP;
  ds.front.compareOp = VK_COMPARE_OP_ALWAYS;
  ds.back = ds.front;
  ds.minDepthBounds = 0.0f;
  ds.maxDepthBounds = 1.0f;

  VkPipelineMultisampleStateCreateInfo ms{VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO};
  ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

  VkGraphicsPipelineCreateInfo pipeInfo{VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO};
  pipeInfo.stageCount = (uint32_t)sp.shaderInfo.size();
  pipeInfo.pStages = sp.shaderInfo.data();
  pipeInfo.pVertexInputState = &vi;
  pipeInfo.pInputAssemblyState = &ia;
  pipeInfo.pTessellationState = nullptr;
  pipeInfo.pViewportState = &vp;
  pipeInfo.pRasterizationState = &rs;
  pipeInfo.pMultisampleState = &ms;
  pipeInfo.pDepthStencilState = &ds;
  pipeInfo.pColorBlendState = &cb;
  if (dynamicState.dynamicStateCount > 0) {
    pipeInfo.pDynamicState = &dynamicState;
  }
  pipeInfo.layout = layout.layout;
  pipeInfo.renderPass = rp.pass;
  pipeInfo.subpass = 0;
  VK_CHECK(vkCreateGraphicsPipelines(m_vkDevice, VK_NULL_HANDLE, 1, &pipeInfo, nullptr, &pipe));
}

void Pipeline::Reset() {
  if (m_vkDevice != nullptr) {
    if (pipe != VK_NULL_HANDLE) {
      vkDestroyPipeline(m_vkDevice, pipe, nullptr);
    }
  }
  pipe = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

void DepthBuffer::Reset() {
  if (m_vkDevice != nullptr) {
    if (depthImage != VK_NULL_HANDLE) {
      vkDestroyImage(m_vkDevice, depthImage, nullptr);
    }
    if (depthMemory != VK_NULL_HANDLE) {
      vkFreeMemory(m_vkDevice, depthMemory, nullptr);
    }
  }
  depthImage = VK_NULL_HANDLE;
  depthMemory = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

void DepthBuffer::Create(VkDevice device, MemoryAllocator* memAllocator, VkFormat depthFormat) {
  m_vkDevice = device;

  VkExtent2D size = {WIDTH, HEIGHT};
  printf("Creating a depth buffer with %dx%d\n", size.width, size.height);

  VkImageCreateInfo imageInfo{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent.width = size.width;
  imageInfo.extent.height = size.height;
  imageInfo.extent.depth = 1;
  imageInfo.mipLevels = 1;
  imageInfo.arrayLayers = 1;
  imageInfo.format = depthFormat;
  imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
  imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  VK_CHECK(vkCreateImage(device, &imageInfo, nullptr, &depthImage));

  VkMemoryRequirements memRequirements{};
  vkGetImageMemoryRequirements(device, depthImage, &memRequirements);
  memAllocator->Allocate(memRequirements, &depthMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  VK_CHECK(vkBindImageMemory(device, depthImage, depthMemory, 0));
}

void Example::BindRenderTarget(VkImage img, uint32_t arraySlice, const VkRect2D& renderArea,
                               VkRenderPassBeginInfo* renderPassBeginInfo, uint32_t index) {
  if (renderTargets.at(index).fb == VK_NULL_HANDLE) {
    renderTargets.at(index).Create(m_vkDevice, img, depthBuffer.depthImage, arraySlice, rp);
  }
  renderPassBeginInfo->renderPass = rp.pass;
  renderPassBeginInfo->framebuffer = renderTargets.at(index).fb;
  renderPassBeginInfo->renderArea = renderArea;
}

void Example::BindPipeline(VkCommandBuffer buf) {
  vkCmdBindPipeline(buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipe.pipe);
}

void Swapchain::Create(VkInstance instance, VkPhysicalDevice physDevice, VkDevice device,
                       uint32_t queueFamilyIndex) {
  m_vkInstance = instance;
  m_vkPhysicalDevice = physDevice;
  m_vkDevice = device;
  m_queueFamilyIndex = queueFamilyIndex;

  xcb_connection_t* connection;
  xcb_window_t window;
  xcb_screen_t* screen;

  connection = xcb_connect(NULL, NULL);

  xcb_screen_iterator_t iter = xcb_setup_roots_iterator(xcb_get_setup(connection));

  screen = iter.data;

  window = xcb_generate_id(connection);

  int x = 0;
  int y = 0;
  int width = size.width;
  int height = size.height;

  printf("Creating xcb window width size %dx%d\n", width, height);

  uint32_t value_list = XCB_EVENT_MASK_STRUCTURE_NOTIFY;

  xcb_create_window(connection, XCB_COPY_FROM_PARENT, window, screen->root, x, y, width, height, 0,
                    XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual, XCB_CW_EVENT_MASK,
                    &value_list);

  xcb_map_window(connection, window);

  VkXcbSurfaceCreateInfoKHR surface_info = {
      .sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
      .connection = connection,
      .window = window,
  };

  VK_CHECK(vkCreateXcbSurfaceKHR(instance, &surface_info, NULL, &surface));

  VkSurfaceCapabilitiesKHR surfCaps;
  VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_vkPhysicalDevice, surface, &surfCaps));
  assert(surfCaps.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT);

  uint32_t surfFmtCount = 0;
  VK_CHECK(
      vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkPhysicalDevice, surface, &surfFmtCount, nullptr));
  std::vector<VkSurfaceFormatKHR> surfFmts(surfFmtCount);
  VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkPhysicalDevice, surface, &surfFmtCount,
                                                &surfFmts[0]));
  uint32_t foundFmt;
  for (foundFmt = 0; foundFmt < surfFmtCount; ++foundFmt) {
    if (surfFmts[foundFmt].format == format) break;
  }

  assert(foundFmt < surfFmtCount);

  uint32_t presentModeCount = 0;
  VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(m_vkPhysicalDevice, surface, &presentModeCount,
                                                     nullptr));
  std::vector<VkPresentModeKHR> presentModes(presentModeCount);
  VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(m_vkPhysicalDevice, surface, &presentModeCount,
                                                     &presentModes[0]));

  VkPresentModeKHR presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
  for (uint32_t i = 0; i < presentModeCount; ++i) {
    if ((presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR) ||
        (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)) {
      presentMode = presentModes[i];
      break;
    }
  }

  VkBool32 presentable = false;
  VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(m_vkPhysicalDevice, m_queueFamilyIndex, surface,
                                                &presentable));
  assert(presentable);

  uint32_t indices[] = {0};

  VkSwapchainCreateInfoKHR swapchainInfo{VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
  swapchainInfo.flags = 0;
  swapchainInfo.surface = surface;
  swapchainInfo.minImageCount = surfCaps.minImageCount;
  swapchainInfo.imageFormat = surfFmts[foundFmt].format;
  swapchainInfo.imageColorSpace = surfFmts[foundFmt].colorSpace;
  swapchainInfo.imageExtent = size;
  swapchainInfo.imageArrayLayers = 1;
  swapchainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  swapchainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  swapchainInfo.queueFamilyIndexCount = 0;
  swapchainInfo.pQueueFamilyIndices = indices,
  swapchainInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
  swapchainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  swapchainInfo.presentMode = presentMode;
  swapchainInfo.clipped = true;
  swapchainInfo.oldSwapchain = VK_NULL_HANDLE;
  VK_CHECK(vkCreateSwapchainKHR(m_vkDevice, &swapchainInfo, nullptr, &swapchain));

  printf("Creating swaptchain with size %dx%d\n", size.width, size.height);

  VkFenceCreateInfo fenceInfo{VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  VK_CHECK(vkCreateFence(m_vkDevice, &fenceInfo, nullptr, &readyFence));

  swapchainCount = 0;
  VK_CHECK(vkGetSwapchainImagesKHR(m_vkDevice, swapchain, &swapchainCount, nullptr));
  assert(swapchainCount < maxImages);
  VK_CHECK(vkGetSwapchainImagesKHR(m_vkDevice, swapchain, &swapchainCount, image));
  if (swapchainCount > maxImages) {
    swapchainCount = maxImages;
  }
}

void Swapchain::Prepare(VkCommandBuffer buf) {
  for (uint32_t i = 0; i < swapchainCount; ++i) {
    VkImageMemoryBarrier imgBarrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
    imgBarrier.srcAccessMask = 0;  // XXX was VK_ACCESS_TRANSFER_READ_BIT wrong?
    imgBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imgBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imgBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    imgBarrier.image = image[i];
    imgBarrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
    vkCmdPipelineBarrier(buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0,
                         nullptr, 0, nullptr, 1, &imgBarrier);
  }
}

void Swapchain::Wait() {
  if (presentFence) {
    VK_CHECK(vkWaitForFences(m_vkDevice, 1, &presentFence, VK_TRUE, UINT64_MAX));
    VK_CHECK(vkResetFences(m_vkDevice, 1, &presentFence));
    presentFence = VK_NULL_HANDLE;
  }
}

void Swapchain::Acquire(VkSemaphore readySemaphore) {
  if (readySemaphore == VK_NULL_HANDLE) {
    Wait();
    presentFence = readyFence;
  }

  VK_CHECK(vkAcquireNextImageKHR(m_vkDevice, swapchain, UINT64_MAX, readySemaphore, presentFence,
                                 &renderImageIdx));
}

void Swapchain::Present(VkQueue queue, VkSemaphore drawComplete) {
  VkPresentInfoKHR presentInfo{VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
  if (drawComplete) {
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &drawComplete;
  }
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = &swapchain;

  presentInfo.pImageIndices = &renderImageIdx;

  auto res = vkQueuePresentKHR(queue, &presentInfo);
  if (res == VK_ERROR_OUT_OF_DATE_KHR) {
    return;
  }
  VK_CHECK(res);
}

static bool _find_physical_device(VkInstance instance, VkPhysicalDevice* device) {
  VkResult ret;
  uint32_t count;

  ret = vkEnumeratePhysicalDevices(instance, &count, NULL);
  if (ret != VK_SUCCESS || count == 0) {
    return false;
  }

  VkPhysicalDevice* phys = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice) * count);
  ret = vkEnumeratePhysicalDevices(instance, &count, phys);
  if (ret != VK_SUCCESS || count == 0) {
    free(phys);
    return false;
  }

  uint32_t gpu_index = 0;
  for (uint32_t i = 0; i < count; i++) {
    VkPhysicalDeviceProperties pdp;
    vkGetPhysicalDeviceProperties(phys[i], &pdp);
    if (pdp.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
      gpu_index = i;
    }
  }

  *device = phys[gpu_index];

  free(phys);

  return true;
}

bool Example::InitializeDevice() {
  {
    std::vector<const char*> extensions = {"VK_KHR_surface", VK_KHR_XCB_SURFACE_EXTENSION_NAME};

    VkApplicationInfo appInfo{VK_STRUCTURE_TYPE_APPLICATION_INFO};
    appInfo.pApplicationName = "hiz_bug";
    appInfo.applicationVersion = 1;
    appInfo.pEngineName = "hiz_bug";
    appInfo.engineVersion = 1;
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instInfo{VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
    instInfo.pApplicationInfo = &appInfo;
    instInfo.enabledExtensionCount = (uint32_t)extensions.size();
    instInfo.ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data();

    VK_CHECK(vkCreateInstance(&instInfo, nullptr, &m_vkInstance));
  }

  assert(_find_physical_device(m_vkInstance, &m_vkPhysicalDevice));

  VkDeviceQueueCreateInfo queueInfo{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO};
  float queuePriorities = 0;
  queueInfo.queueCount = 1;
  queueInfo.pQueuePriorities = &queuePriorities;

  uint32_t queueFamilyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount, nullptr);
  std::vector<VkQueueFamilyProperties> queueFamilyProps(queueFamilyCount);
  vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount,
                                           &queueFamilyProps[0]);

  for (uint32_t i = 0; i < queueFamilyCount; ++i) {
    if ((queueFamilyProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0u) {
      m_queueFamilyIndex = queueInfo.queueFamilyIndex = i;
      break;
    }
  }

  std::vector<const char*> deviceExtensions = {
      "VK_KHR_swapchain",
  };

  VkDeviceCreateInfo deviceInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
  deviceInfo.flags = VkDeviceCreateFlags(0);
  deviceInfo.queueCreateInfoCount = 1;
  deviceInfo.pQueueCreateInfos = &queueInfo;
  deviceInfo.enabledLayerCount = 0;
  deviceInfo.ppEnabledLayerNames = nullptr;
  deviceInfo.enabledExtensionCount = (uint32_t)deviceExtensions.size();
  deviceInfo.ppEnabledExtensionNames = deviceExtensions.empty() ? nullptr : deviceExtensions.data();

  VK_CHECK(vkCreateDevice(m_vkPhysicalDevice, &deviceInfo, nullptr, &m_vkDevice));

  vkGetDeviceQueue(m_vkDevice, queueInfo.queueFamilyIndex, 0, &m_vkQueue);

  m_memAllocator.Init(m_vkPhysicalDevice, m_vkDevice);

  InitializeResources();
  return true;
}

// Compile a shader to a SPIR-V binary.
std::vector<uint32_t> Example::CompileGlslShader(const std::string& name, shaderc_shader_kind kind,
                                                 const std::string& source) {
  shaderc::Compiler compiler;
  shaderc::CompileOptions options;

  options.SetOptimizationLevel(shaderc_optimization_level_size);

  shaderc::SpvCompilationResult module =
      compiler.CompileGlslToSpv(source, kind, name.c_str(), options);

  if (module.GetCompilationStatus() != shaderc_compilation_status_success) {
    THROW("Shader compilation failed");
  }

  return {module.cbegin(), module.cend()};
}

void Example::InitializeResources() {
  auto vertexSPIRV =
      CompileGlslShader("vertex", shaderc_glsl_default_vertex_shader, VertexShaderGlsl);
  auto fragmentSPIRV =
      CompileGlslShader("fragment", shaderc_glsl_default_fragment_shader, FragmentShaderGlsl);

  if (vertexSPIRV.empty()) THROW("Failed to compile vertex shader");
  if (fragmentSPIRV.empty()) THROW("Failed to compile fragment shader");

  m_shaderProgram.Init(m_vkDevice);
  m_shaderProgram.LoadVertexShader(vertexSPIRV);
  m_shaderProgram.LoadFragmentShader(fragmentSPIRV);

  // Semaphore to block on draw complete
  VkSemaphoreCreateInfo semInfo{VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
  VK_CHECK(vkCreateSemaphore(m_vkDevice, &semInfo, nullptr, &m_vkDrawDone));

  if (!m_cmdBuffer.Init(m_vkDevice, m_queueFamilyIndex)) THROW("Failed to create command buffer");

  m_pipelineLayout.Create(m_vkDevice);

  static_assert(sizeof(Vertex) == 24, "Unexpected Vertex size");
  m_drawBuffer.Init(m_vkDevice, &m_memAllocator,
                    {{0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, Position)},
                     {1, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, Color)}});
  uint32_t numCubeIdicies = sizeof(c_cubeIndices) / sizeof(c_cubeIndices[0]);
  uint32_t numCubeVerticies = sizeof(c_cubeVertices) / sizeof(c_cubeVertices[0]);
  m_drawBuffer.Create(numCubeIdicies, numCubeVerticies);
  m_drawBuffer.UpdateIndicies(c_cubeIndices.data(), numCubeIdicies, 0);
  m_drawBuffer.UpdateVertices(c_cubeVertices.data(), numCubeVerticies, 0);

  m_swapchain.Create(m_vkInstance, m_vkPhysicalDevice, m_vkDevice, m_queueFamilyIndex);

  renderTargets = std::vector<RenderTarget>(m_swapchain.swapchainCount);

  depthBuffer.Create(m_vkDevice, &m_memAllocator, VK_FORMAT_D32_SFLOAT);
  rp.Create(m_vkDevice, VK_FORMAT_B8G8R8A8_SRGB, VK_FORMAT_D32_SFLOAT);
  pipe.Dynamic(VK_DYNAMIC_STATE_SCISSOR);
  pipe.Dynamic(VK_DYNAMIC_STATE_VIEWPORT);
  pipe.Create(m_vkDevice, m_pipelineLayout, rp, m_shaderProgram, m_drawBuffer);

  m_cmdBuffer.Clear();
  m_cmdBuffer.Begin();
  m_swapchain.Prepare(m_cmdBuffer.buf);
  m_cmdBuffer.End();
  m_cmdBuffer.Exec(m_vkQueue);
  m_cmdBuffer.Wait();
}

void Example::ShutdownDevice() {
  if (m_vkDevice != VK_NULL_HANDLE) {
    vkDeviceWaitIdle(m_vkDevice);

    m_queueFamilyIndex = 0;
    m_vkQueue = VK_NULL_HANDLE;
    if (m_vkDrawDone) {
      vkDestroySemaphore(m_vkDevice, m_vkDrawDone, nullptr);
      m_vkDrawDone = VK_NULL_HANDLE;
    }

    renderTargets.clear();
    depthBuffer.Reset();
    pipe.Reset();
    rp.Reset();

    m_drawBuffer.Reset();
    m_cmdBuffer.Reset();
    m_pipelineLayout.Reset();
    m_shaderProgram.Reset();
    m_memAllocator.Reset();

    m_swapchain.Reset();
    vkDestroyDevice(m_vkDevice, nullptr);
    m_vkDevice = VK_NULL_HANDLE;
  }
  if (m_vkInstance != VK_NULL_HANDLE) {
    vkDestroyInstance(m_vkInstance, nullptr);
    m_vkInstance = VK_NULL_HANDLE;
  }
}

void Example::SetViewportAndScissor(const VkRect2D& rect) {
  VkViewport viewport{float(rect.offset.x),
                      float(rect.offset.y),
                      float(rect.extent.width),
                      float(rect.extent.height),
                      0.0f,
                      1.0f};
  vkCmdSetViewport(m_cmdBuffer.buf, 0, 1, &viewport);
  vkCmdSetScissor(m_cmdBuffer.buf, 0, 1, &rect);
}

void Example::ClearImageSlice(uint32_t imageArrayIndex, int imageIndex) {
  m_cmdBuffer.Clear();
  m_cmdBuffer.Begin();

  VkRect2D renderArea = {{0, 0}, {WIDTH, HEIGHT}};
  SetViewportAndScissor(renderArea);

  VkRenderPassBeginInfo renderPassBeginInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
  BindRenderTarget(m_swapchain.image[imageIndex], imageArrayIndex, renderArea, &renderPassBeginInfo,
                   imageIndex);
  vkCmdBeginRenderPass(m_cmdBuffer.buf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  BindPipeline(m_cmdBuffer.buf);

  static std::array<VkClearValue, 2> clearValues;
  clearValues[0].color.float32[0] = 0.184313729f;
  clearValues[0].color.float32[1] = 0.309803933f;
  clearValues[0].color.float32[2] = 0.309803933f;
  clearValues[0].color.float32[3] = 1.0f;
  clearValues[1].depthStencil.depth = 1.0f;
  clearValues[1].depthStencil.stencil = 0;
  std::array<VkClearAttachment, 2> clearAttachments{{
      {VK_IMAGE_ASPECT_COLOR_BIT, 0, clearValues[0]},
      {VK_IMAGE_ASPECT_DEPTH_BIT, 0, clearValues[1]},
  }};
  VkClearRect clearRect{renderArea, 0, 1};
  vkCmdClearAttachments(m_cmdBuffer.buf, 2, &clearAttachments[0], 1, &clearRect);

  vkCmdEndRenderPass(m_cmdBuffer.buf);

  m_cmdBuffer.End();
  m_cmdBuffer.Exec(m_vkQueue);
  m_cmdBuffer.Wait();
}

void Example::RenderView(const std::vector<Cube>& cubes, int imageIndex) {
  m_cmdBuffer.Clear();
  m_cmdBuffer.Begin();

  VkRect2D renderArea = {{0, 0}, {WIDTH, HEIGHT}};

  SetViewportAndScissor(renderArea);

  VkRenderPassBeginInfo renderPassBeginInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};

  BindRenderTarget(m_swapchain.image[imageIndex], 0, renderArea, &renderPassBeginInfo, imageIndex);

  vkCmdBeginRenderPass(m_cmdBuffer.buf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  BindPipeline(m_cmdBuffer.buf);

  vkCmdBindIndexBuffer(m_cmdBuffer.buf, m_drawBuffer.idxBuf, 0, VK_INDEX_TYPE_UINT16);

  VkDeviceSize offset = 0;
  vkCmdBindVertexBuffers(m_cmdBuffer.buf, 0, 1, &m_drawBuffer.vtxBuf, &offset);

  XrPosef pose = {.orientation = {.x = 0, .y = 0, .z = 0, .w = 1},
                  .position = {.x = 0, .y = 0, .z = 0}};

  XrFovf fov = {-0.8, 0.8, 0.6, -0.6};

  XrMatrix4x4f proj;
  XrMatrix4x4f_CreateProjectionFov(&proj, fov, 0.05f, 100.0f);
  XrMatrix4x4f toView;
  XrVector3f scale{1.f, 1.f, 1.f};
  XrMatrix4x4f_CreateTranslationRotationScale(&toView, &pose.position, &pose.orientation, &scale);
  XrMatrix4x4f view;
  XrMatrix4x4f_InvertRigidBody(&view, &toView);
  XrMatrix4x4f vp;
  XrMatrix4x4f_Multiply(&vp, &proj, &view);

  for (const Cube& cube : cubes) {
    XrMatrix4x4f model;
    XrMatrix4x4f_CreateTranslationRotationScale(&model, &cube.Pose.position, &cube.Pose.orientation,
                                                &cube.Scale);
    XrMatrix4x4f mvp;
    XrMatrix4x4f_Multiply(&mvp, &vp, &model);
    vkCmdPushConstants(m_cmdBuffer.buf, m_pipelineLayout.layout, VK_SHADER_STAGE_VERTEX_BIT, 0,
                       sizeof(mvp.m), &mvp.m[0]);

    vkCmdDrawIndexed(m_cmdBuffer.buf, m_drawBuffer.count.idx, 1, 0, 0, 0);
  }

  vkCmdEndRenderPass(m_cmdBuffer.buf);

  m_cmdBuffer.End();
  m_cmdBuffer.Exec(m_vkQueue);
  m_cmdBuffer.Wait();

  m_swapchain.Present(m_vkQueue);
}

int main() {
  Example example;

  assert(example.InitializeDevice());

  const std::vector<Cube> cubes = {Cube::Make({-1, 0, -2}), Cube::Make({1, 0, -2}),
                                   Cube::Make({0, -1, -2}), Cube::Make({0, 1, -2})};

  uint32_t rendered_frames = 0;
  while (rendered_frames < 10000) {
    example.m_swapchain.Acquire();

    example.ClearImageSlice(0, example.m_swapchain.renderImageIdx);
    example.RenderView(cubes, example.m_swapchain.renderImageIdx);

    rendered_frames++;
  }

  example.ShutdownDevice();

  return 0;
}
