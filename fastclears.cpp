// Copyright (c) 2017-2020 The Khronos Group Inc.
// Copyright (c) 2020 Collabora Ltd.
// SPDX-License-Identifier: Apache-2.0

#include <assert.h>
#include <string.h>

#include <algorithm>
#define XR_CHECK(result) assert(result == XR_SUCCESS)
#define XR_CHECK(result) assert(result == XR_SUCCESS)
#define XRC_THROW(message) printf(message);

#include <assert.h>
#include <openxr/openxr.h>

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>
#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>

#define USE_ONLINE_VULKAN_SHADERC
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <list>
#include <shaderc/shaderc.hpp>

#define VK_CHECK(result) assert(result == VK_SUCCESS)

struct CmdBuffer {
  enum class CmdBufferState { Undefined, Initialized, Recording, Executable, Executing };

  CmdBufferState state{CmdBufferState::Undefined};
  VkCommandPool pool{VK_NULL_HANDLE};
  VkCommandBuffer buf{VK_NULL_HANDLE};
  VkFence execFence{VK_NULL_HANDLE};

  CmdBuffer() = default;
  CmdBuffer(const CmdBuffer&) = delete;
  CmdBuffer& operator=(const CmdBuffer&) = delete;
  CmdBuffer(CmdBuffer&&) = delete;
  CmdBuffer& operator=(CmdBuffer&&) = delete;
  ~CmdBuffer();
  void Reset();
  bool Init(VkDevice device, uint32_t queueFamilyIndex);
  bool Begin();
  bool End();
  bool Exec(VkQueue queue);
  bool Wait();
  bool Clear();

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};

  void SetState(CmdBufferState newState) { state = newState; }
};

struct RenderPass {
  VkFormat colorFmt{};
  VkFormat depthFmt{};
  VkRenderPass pass{VK_NULL_HANDLE};

  RenderPass() = default;

  bool Create(VkDevice device, VkFormat aColorFmt, VkFormat aDepthFmt);

  void Reset();

  ~RenderPass() { Reset(); }

  RenderPass(const RenderPass&) = delete;
  RenderPass& operator=(const RenderPass&) = delete;
  RenderPass(RenderPass&&) = delete;
  RenderPass& operator=(RenderPass&&) = delete;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct RenderTarget {
  VkImage colorImage{VK_NULL_HANDLE};
  VkImage depthImage{VK_NULL_HANDLE};
  VkImageView colorView{VK_NULL_HANDLE};
  VkImageView depthView{VK_NULL_HANDLE};
  VkFramebuffer fb{VK_NULL_HANDLE};

  RenderTarget() = default;
  ~RenderTarget();

  void Create(VkDevice device, VkImage aColorImage, VkImage aDepthImage, uint32_t baseArrayLayer,
              VkExtent2D size, RenderPass& renderPass);

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct MemoryAllocator {
  void Init(VkPhysicalDevice physicalDevice, VkDevice device);

  void Reset();
  static const VkFlags defaultFlags =
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  void Allocate(VkMemoryRequirements const& memReqs, VkDeviceMemory* mem,
                VkFlags flags = defaultFlags, const void* pNext = nullptr) const;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  VkPhysicalDeviceMemoryProperties m_memProps{};
};

struct DepthBuffer {
  VkDeviceMemory depthMemory{VK_NULL_HANDLE};
  VkImage depthImage{VK_NULL_HANDLE};

  DepthBuffer() = default;
  ~DepthBuffer() { Reset(); }

  void Reset();

  void Create(VkDevice device, MemoryAllocator* memAllocator, VkFormat depthFormat,
              const XrSwapchainCreateInfo& swapchainCreateInfo);

  DepthBuffer(const DepthBuffer&) = delete;
  DepthBuffer& operator=(const DepthBuffer&) = delete;

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct SwapchainImageContext {
  std::vector<XrSwapchainImageBaseHeader*> imagePtrVector;
  std::vector<XrSwapchainImageVulkanKHR> swapchainImages;
  VkExtent2D size{};
  class ArraySliceState {
   public:
    ArraySliceState() {}
    ArraySliceState(const ArraySliceState&) {}
    std::vector<RenderTarget> renderTarget;  // per swapchain index
    DepthBuffer depthBuffer{};
    RenderPass rp{};
  };
  std::vector<ArraySliceState> slice{};

  SwapchainImageContext() = default;
  ~SwapchainImageContext() { Reset(); }

  std::vector<XrSwapchainImageBaseHeader*> Create(VkDevice device, MemoryAllocator* memAllocator,
                                                  uint32_t capacity,
                                                  const XrSwapchainCreateInfo& swapchainCreateInfo);

  void Reset();

  uint32_t ImageIndex(const XrSwapchainImageBaseHeader* swapchainImageHeader);

  void BindRenderTarget(uint32_t index, uint32_t arraySlice, const VkRect2D& renderArea,
                        VkRenderPassBeginInfo* renderPassBeginInfo);

 private:
  VkDevice m_vkDevice{VK_NULL_HANDLE};
};

struct Example {
  Example(){};

  std::vector<const char*> ParseExtensionString(char* names) {
    std::vector<const char*> list;
    while (*names != 0) {
      list.push_back(names);
      while (*(++names) != 0) {
        if (*names == ' ') {
          *names++ = '\0';
          break;
        }
      }
    }
    return list;
  }

  bool InitializeDevice(XrInstance instance, XrSystemId systemId);

  void InitializeResources();

  void ShutdownDevice();

  void InitXR();
  void KillXR();

  const XrBaseInStructure* GetGraphicsBinding() const;

  int64_t SelectColorSwapchainFormat(const int64_t* imageFormatArray, size_t count) const;

  std::shared_ptr<SwapchainImageContext> AllocateSwapchainImageStructs(
      size_t size, const XrSwapchainCreateInfo& swapchainCreateInfo);

  void SetViewportAndScissor(const VkRect2D& rect);

  void ClearImageSlice(const XrSwapchainImageBaseHeader* colorSwapchainImage,
                       uint32_t imageArrayIndex);

  void RenderView(const XrCompositionLayerProjectionView& layerView,
                  const XrSwapchainImageBaseHeader* colorSwapchainImage);

 private:
  XrGraphicsBindingVulkanKHR m_graphicsBinding{XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR};
  std::map<const XrSwapchainImageBaseHeader*, std::shared_ptr<SwapchainImageContext>>
      m_swapchainImageContextMap;

  VkInstance m_vkInstance{VK_NULL_HANDLE};
  VkPhysicalDevice m_vkPhysicalDevice{VK_NULL_HANDLE};
  VkDevice m_vkDevice{VK_NULL_HANDLE};
  uint32_t m_queueFamilyIndex = 0;
  VkQueue m_vkQueue{VK_NULL_HANDLE};
  VkSemaphore m_vkDrawDone{VK_NULL_HANDLE};

  MemoryAllocator m_memAllocator{};
  CmdBuffer m_cmdBuffer{};

 private:
  XrInstance m_instance;
  XrSession m_session;
  XrSystemId m_systemId;
  uint64_t m_defaultColorFormat;
  XrViewConfigurationType m_primaryViewType;
  uint32_t m_projectionViewCount{0};
  std::list<XrCompositionLayerProjection> m_projections;
  std::list<std::vector<XrCompositionLayerProjectionView>> m_projectionViews;
  std::map<XrSwapchain, XrSwapchainCreateInfo> m_createdSwapchains;
  std::map<XrSwapchain, std::shared_ptr<SwapchainImageContext>> m_swapchainImages;
  std::vector<XrSpace> m_spaces;
  XrSpace m_localSpace;
  XrCompositionLayerProjection* m_projLayer;
  std::vector<XrSwapchain> m_swapchains;

  XrResult CreateBasicSession(XrInstance instance, XrSystemId* systemId, XrSession* session,
                              bool enableGraphicsSystem = true);
  std::vector<XrViewConfigurationView> EnumerateConfigurationViews();
  void BeginSession();
  std::tuple<XrViewState, std::vector<XrView>> LocateViews(XrSpace space, int64_t displayTime);
  void EndFrame(XrTime predictedDisplayTime);
  void AcquireWaitReleaseImage(
      XrSwapchain swapchain,
      std::function<void(const XrSwapchainImageBaseHeader*)> doUpdate);
  XrSpace CreateReferenceSpace(XrReferenceSpaceType type);
  XrSwapchainCreateInfo DefaultColorSwapchainCreateInfo(uint32_t width, uint32_t height,
                                                        XrSwapchainCreateFlags createFlags = 0,
                                                        int64_t format = -1);
  XrSwapchain CreateSwapchain(const XrSwapchainCreateInfo& createInfo);
  XrSwapchainSubImage MakeDefaultSubImage(XrSwapchain swapchain, uint32_t imageArrayIndex = 0);
  XrCompositionLayerProjection* CreateProjectionLayer(XrSpace space);
  void InitProjectionLayerHelper();
  void UpdateProjectionLayer(const XrFrameState& frameState);
  bool IterateFrame();
  void Loop();
};

void MemoryAllocator::Init(VkPhysicalDevice physicalDevice, VkDevice device) {
  m_vkDevice = device;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &m_memProps);
}

void MemoryAllocator::Reset() {
  m_memProps = {};
  m_vkDevice = VK_NULL_HANDLE;
}

void MemoryAllocator::Allocate(VkMemoryRequirements const& memReqs, VkDeviceMemory* mem,
                               VkFlags flags, const void* pNext) const {
  for (uint32_t i = 0; i < m_memProps.memoryTypeCount; ++i) {
    if ((memReqs.memoryTypeBits & (1 << i)) != 0u) {
      if ((m_memProps.memoryTypes[i].propertyFlags & flags) == flags) {
        VkMemoryAllocateInfo memAlloc{VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, pNext};
        memAlloc.allocationSize = memReqs.size;
        memAlloc.memoryTypeIndex = i;
        VK_CHECK(vkAllocateMemory(m_vkDevice, &memAlloc, nullptr, mem));
        return;
      }
    }
  }
  XRC_THROW("Memory format not supported");
}

void CmdBuffer::Reset() {
  SetState(CmdBufferState::Undefined);
  if (m_vkDevice != nullptr) {
    if (buf != VK_NULL_HANDLE) {
      vkFreeCommandBuffers(m_vkDevice, pool, 1, &buf);
    }
    if (pool != VK_NULL_HANDLE) {
      vkDestroyCommandPool(m_vkDevice, pool, nullptr);
    }
    if (execFence != VK_NULL_HANDLE) {
      vkDestroyFence(m_vkDevice, execFence, nullptr);
    }
  }
  buf = VK_NULL_HANDLE;
  pool = VK_NULL_HANDLE;
  execFence = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

CmdBuffer::~CmdBuffer() { Reset(); }

bool CmdBuffer::Init(VkDevice device, uint32_t queueFamilyIndex) {
  assert((state == CmdBufferState::Undefined) || (state == CmdBufferState::Initialized));

  m_vkDevice = device;

  VkCommandPoolCreateInfo cmdPoolInfo{VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
  cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  cmdPoolInfo.queueFamilyIndex = queueFamilyIndex;
  VK_CHECK(vkCreateCommandPool(m_vkDevice, &cmdPoolInfo, nullptr, &pool));

  VkCommandBufferAllocateInfo cmd{VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
  cmd.commandPool = pool;
  cmd.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  cmd.commandBufferCount = 1;
  VK_CHECK(vkAllocateCommandBuffers(m_vkDevice, &cmd, &buf));

  VkFenceCreateInfo fenceInfo{VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  VK_CHECK(vkCreateFence(m_vkDevice, &fenceInfo, nullptr, &execFence));

  SetState(CmdBufferState::Initialized);
  return true;
}

bool CmdBuffer::Begin() {
  assert(state == CmdBufferState::Initialized);
  VkCommandBufferBeginInfo cmdBeginInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
  VK_CHECK(vkBeginCommandBuffer(buf, &cmdBeginInfo));
  SetState(CmdBufferState::Recording);
  return true;
}

bool CmdBuffer::End() {
  assert(state == CmdBufferState::Recording);
  VK_CHECK(vkEndCommandBuffer(buf));
  SetState(CmdBufferState::Executable);
  return true;
}

bool CmdBuffer::Exec(VkQueue queue) {
  assert(state == CmdBufferState::Executable);

  VkSubmitInfo submitInfo{VK_STRUCTURE_TYPE_SUBMIT_INFO};
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &buf;
  VK_CHECK(vkQueueSubmit(queue, 1, &submitInfo, execFence));

  SetState(CmdBufferState::Executing);
  return true;
}

bool CmdBuffer::Wait() {
  if (state == CmdBufferState::Initialized) {
    return true;
  }

  assert(state == CmdBufferState::Executing);

  const uint32_t timeoutNs = 1 * 1000 * 1000 * 1000;
  for (int i = 0; i < 5; ++i) {
    auto res = vkWaitForFences(m_vkDevice, 1, &execFence, VK_TRUE, timeoutNs);
    if (res == VK_SUCCESS) {
      SetState(CmdBufferState::Executable);
      return true;
    }
  }

  return false;
}

bool CmdBuffer::Clear() {
  if (state != CmdBufferState::Initialized) {
    assert(state == CmdBufferState::Executable);

    VK_CHECK(vkResetFences(m_vkDevice, 1, &execFence));
    VK_CHECK(vkResetCommandBuffer(buf, 0));

    SetState(CmdBufferState::Initialized);
  }

  return true;
}

bool RenderPass::Create(VkDevice device, VkFormat aColorFmt, VkFormat aDepthFmt) {
  m_vkDevice = device;
  colorFmt = aColorFmt;
  depthFmt = aDepthFmt;

  VkSubpassDescription subpass = {};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

  VkAttachmentReference colorRef = {0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
  VkAttachmentReference depthRef = {1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};

  std::array<VkAttachmentDescription, 2> at = {};

  VkRenderPassCreateInfo rpInfo{VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
  rpInfo.attachmentCount = 0;
  rpInfo.pAttachments = at.data();
  rpInfo.subpassCount = 1;
  rpInfo.pSubpasses = &subpass;

  if (colorFmt != VK_FORMAT_UNDEFINED) {
    colorRef.attachment = rpInfo.attachmentCount++;

    at[colorRef.attachment].format = colorFmt;
    at[colorRef.attachment].samples = VK_SAMPLE_COUNT_1_BIT;
    at[colorRef.attachment].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    at[colorRef.attachment].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    at[colorRef.attachment].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    at[colorRef.attachment].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    at[colorRef.attachment].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    at[colorRef.attachment].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorRef;
  }

  if (depthFmt != VK_FORMAT_UNDEFINED) {
    depthRef.attachment = rpInfo.attachmentCount++;

    at[depthRef.attachment].format = depthFmt;
    at[depthRef.attachment].samples = VK_SAMPLE_COUNT_1_BIT;
    at[depthRef.attachment].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    at[depthRef.attachment].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    at[depthRef.attachment].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    at[depthRef.attachment].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    at[depthRef.attachment].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    at[depthRef.attachment].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    subpass.pDepthStencilAttachment = &depthRef;
  }

  VK_CHECK(vkCreateRenderPass(m_vkDevice, &rpInfo, nullptr, &pass));

  return true;
}

void RenderPass::Reset() {
  if (m_vkDevice != nullptr) {
    if (pass != VK_NULL_HANDLE) {
      vkDestroyRenderPass(m_vkDevice, pass, nullptr);
    }
  }
  pass = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

RenderTarget::~RenderTarget() {
  if (m_vkDevice != VK_NULL_HANDLE) {
    if (fb != VK_NULL_HANDLE) {
      vkDestroyFramebuffer(m_vkDevice, fb, nullptr);
    }
    if (colorView != VK_NULL_HANDLE) {
      vkDestroyImageView(m_vkDevice, colorView, nullptr);
    }
    if (depthView != VK_NULL_HANDLE) {
      vkDestroyImageView(m_vkDevice, depthView, nullptr);
    }
  }

  colorImage = VK_NULL_HANDLE;
  depthImage = VK_NULL_HANDLE;
  colorView = VK_NULL_HANDLE;
  depthView = VK_NULL_HANDLE;
  fb = VK_NULL_HANDLE;
  m_vkDevice = VK_NULL_HANDLE;
}

void RenderTarget::Create(VkDevice device, VkImage aColorImage, VkImage aDepthImage,
                          uint32_t baseArrayLayer, VkExtent2D size, RenderPass& renderPass) {
  m_vkDevice = device;

  colorImage = aColorImage;
  depthImage = aDepthImage;

  std::array<VkImageView, 2> attachments{};
  uint32_t attachmentCount = 0;

  if (colorImage != VK_NULL_HANDLE) {
    VkImageViewCreateInfo colorViewInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    colorViewInfo.image = colorImage;
    colorViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    colorViewInfo.format = renderPass.colorFmt;
    colorViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    colorViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    colorViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    colorViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    colorViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    colorViewInfo.subresourceRange.baseMipLevel = 0;
    colorViewInfo.subresourceRange.levelCount = 1;
    colorViewInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
    colorViewInfo.subresourceRange.layerCount = 1;
    VK_CHECK(vkCreateImageView(m_vkDevice, &colorViewInfo, nullptr, &colorView));
    attachments[attachmentCount++] = colorView;
  }

  if (depthImage != VK_NULL_HANDLE) {
    VkImageViewCreateInfo depthViewInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    depthViewInfo.image = depthImage;
    depthViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    depthViewInfo.format = renderPass.depthFmt;
    depthViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    depthViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    depthViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    depthViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    depthViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    depthViewInfo.subresourceRange.baseMipLevel = 0;
    depthViewInfo.subresourceRange.levelCount = 1;
    depthViewInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
    depthViewInfo.subresourceRange.layerCount = 1;
    VK_CHECK(vkCreateImageView(m_vkDevice, &depthViewInfo, nullptr, &depthView));
    attachments[attachmentCount++] = depthView;
  }

  VkFramebufferCreateInfo fbInfo{VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
  fbInfo.renderPass = renderPass.pass;
  fbInfo.attachmentCount = attachmentCount;
  fbInfo.pAttachments = attachments.data();
  fbInfo.width = size.width;
  fbInfo.height = size.height;
  fbInfo.layers = 1;
  VK_CHECK(vkCreateFramebuffer(m_vkDevice, &fbInfo, nullptr, &fb));
}

void DepthBuffer::Reset() {
  if (m_vkDevice != nullptr) {
    if (depthImage != VK_NULL_HANDLE) {
      vkDestroyImage(m_vkDevice, depthImage, nullptr);
    }
    if (depthMemory != VK_NULL_HANDLE) {
      vkFreeMemory(m_vkDevice, depthMemory, nullptr);
    }
  }
  depthImage = VK_NULL_HANDLE;
  depthMemory = VK_NULL_HANDLE;
  m_vkDevice = nullptr;
}

void DepthBuffer::Create(VkDevice device, MemoryAllocator* memAllocator, VkFormat depthFormat,
                         const XrSwapchainCreateInfo& swapchainCreateInfo) {
  m_vkDevice = device;

  VkExtent2D size = {swapchainCreateInfo.width, swapchainCreateInfo.height};
  printf("Creating a depth buffer with %dx%d\n", swapchainCreateInfo.width,
         swapchainCreateInfo.height);

  VkImageCreateInfo imageInfo{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent.width = size.width;
  imageInfo.extent.height = size.height;
  imageInfo.extent.depth = 1;
  imageInfo.mipLevels = 1;
  imageInfo.arrayLayers = swapchainCreateInfo.arraySize;
  imageInfo.format = depthFormat;
  imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
  imageInfo.samples = (VkSampleCountFlagBits)swapchainCreateInfo.sampleCount;
  imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  VK_CHECK(vkCreateImage(device, &imageInfo, nullptr, &depthImage));

  VkMemoryRequirements memRequirements{};
  vkGetImageMemoryRequirements(device, depthImage, &memRequirements);
  memAllocator->Allocate(memRequirements, &depthMemory, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  VK_CHECK(vkBindImageMemory(device, depthImage, depthMemory, 0));
}

std::vector<XrSwapchainImageBaseHeader*> SwapchainImageContext::Create(
    VkDevice device, MemoryAllocator* memAllocator, uint32_t capacity,
    const XrSwapchainCreateInfo& swapchainCreateInfo) {
  m_vkDevice = device;

  size = {swapchainCreateInfo.width, swapchainCreateInfo.height};
  VkFormat colorFormat = (VkFormat)swapchainCreateInfo.format;
  VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;

  swapchainImages.resize(capacity);
  std::vector<XrSwapchainImageBaseHeader*> bases(capacity);
  for (uint32_t i = 0; i < capacity; ++i) {
    swapchainImages[i] = {XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR};
    bases[i] = reinterpret_cast<XrSwapchainImageBaseHeader*>(&swapchainImages[i]);
  }

  slice.resize(swapchainCreateInfo.arraySize);
  for (auto& s : slice) {
    s.renderTarget.resize(capacity);
    s.depthBuffer.Create(m_vkDevice, memAllocator, depthFormat, swapchainCreateInfo);
    s.rp.Create(m_vkDevice, colorFormat, depthFormat);
  }

  return bases;
}

void SwapchainImageContext::Reset() {
  if (m_vkDevice) {
    swapchainImages.clear();
    size = {};
    slice.clear();
    m_vkDevice = VK_NULL_HANDLE;
  }
}

uint32_t SwapchainImageContext::ImageIndex(const XrSwapchainImageBaseHeader* swapchainImageHeader) {
  auto p = reinterpret_cast<const XrSwapchainImageVulkanKHR*>(swapchainImageHeader);
  return (uint32_t)(p - &swapchainImages[0]);
}

void SwapchainImageContext::BindRenderTarget(uint32_t index, uint32_t arraySlice,
                                             const VkRect2D& renderArea,
                                             VkRenderPassBeginInfo* renderPassBeginInfo) {
  auto& s = slice[arraySlice];
  RenderTarget& rt = s.renderTarget[index];
  if (rt.fb == VK_NULL_HANDLE) {
    rt.Create(m_vkDevice, swapchainImages[index].image, s.depthBuffer.depthImage, arraySlice, size,
              s.rp);
  }
  renderPassBeginInfo->renderPass = s.rp.pass;
  renderPassBeginInfo->framebuffer = rt.fb;
  renderPassBeginInfo->renderArea = renderArea;
}

bool Example::InitializeDevice(XrInstance instance, XrSystemId systemId) {
  PFN_xrGetVulkanGraphicsRequirementsKHR pfnGetVulkanGraphicsRequirementsKHR = nullptr;
  XR_CHECK(xrGetInstanceProcAddr(
      instance, "xrGetVulkanGraphicsRequirementsKHR",
      reinterpret_cast<PFN_xrVoidFunction*>(&pfnGetVulkanGraphicsRequirementsKHR)));

  PFN_xrGetVulkanInstanceExtensionsKHR pfnGetVulkanInstanceExtensionsKHR = nullptr;
  XR_CHECK(xrGetInstanceProcAddr(
      instance, "xrGetVulkanInstanceExtensionsKHR",
      reinterpret_cast<PFN_xrVoidFunction*>(&pfnGetVulkanInstanceExtensionsKHR)));

  XrGraphicsRequirementsVulkanKHR graphicsRequirements{XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR};
  XR_CHECK(pfnGetVulkanGraphicsRequirementsKHR(instance, systemId, &graphicsRequirements));
  const XrVersion vulkanVersion = XR_MAKE_VERSION(VK_VERSION_MAJOR(VK_API_VERSION_1_0),
                                                  VK_VERSION_MINOR(VK_API_VERSION_1_0), 0);
  if ((vulkanVersion < graphicsRequirements.minApiVersionSupported) ||
      (vulkanVersion > graphicsRequirements.maxApiVersionSupported)) {
    // Log?
    return false;
  }

  uint32_t extensionNamesSize = 0;
  XR_CHECK(pfnGetVulkanInstanceExtensionsKHR(instance, systemId, 0, &extensionNamesSize, nullptr));
  std::vector<char> extensionNames(extensionNamesSize);

  XR_CHECK(pfnGetVulkanInstanceExtensionsKHR(instance, systemId, extensionNamesSize,
                                             &extensionNamesSize, &extensionNames[0]));

  {
    std::vector<const char*> extensions = ParseExtensionString(&extensionNames[0]);

    std::vector<const char*> layers;

    VkApplicationInfo appInfo{VK_STRUCTURE_TYPE_APPLICATION_INFO};
    appInfo.pApplicationName = "fastclears_bug";
    appInfo.applicationVersion = 1;
    appInfo.pEngineName = "fastclears_bug";
    appInfo.engineVersion = 1;
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instInfo{VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
    instInfo.pApplicationInfo = &appInfo;
    instInfo.enabledLayerCount = (uint32_t)layers.size();
    instInfo.ppEnabledLayerNames = layers.empty() ? nullptr : layers.data();
    instInfo.enabledExtensionCount = (uint32_t)extensions.size();
    instInfo.ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data();

    VK_CHECK(vkCreateInstance(&instInfo, nullptr, &m_vkInstance));
  }

  PFN_xrGetVulkanGraphicsDeviceKHR pfnGetVulkanGraphicsDeviceKHR = nullptr;
  XR_CHECK(
      xrGetInstanceProcAddr(instance, "xrGetVulkanGraphicsDeviceKHR",
                            reinterpret_cast<PFN_xrVoidFunction*>(&pfnGetVulkanGraphicsDeviceKHR)));

  XR_CHECK(pfnGetVulkanGraphicsDeviceKHR(instance, systemId, m_vkInstance, &m_vkPhysicalDevice));

  VkDeviceQueueCreateInfo queueInfo{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO};
  float queuePriorities = 0;
  queueInfo.queueCount = 1;
  queueInfo.pQueuePriorities = &queuePriorities;

  uint32_t queueFamilyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount, nullptr);
  std::vector<VkQueueFamilyProperties> queueFamilyProps(queueFamilyCount);
  vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount,
                                           &queueFamilyProps[0]);

  for (uint32_t i = 0; i < queueFamilyCount; ++i) {
    if ((queueFamilyProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0u) {
      m_queueFamilyIndex = queueInfo.queueFamilyIndex = i;
      break;
    }
  }

  PFN_xrGetVulkanDeviceExtensionsKHR pfnGetVulkanDeviceExtensionsKHR = nullptr;
  XR_CHECK(xrGetInstanceProcAddr(
      instance, "xrGetVulkanDeviceExtensionsKHR",
      reinterpret_cast<PFN_xrVoidFunction*>(&pfnGetVulkanDeviceExtensionsKHR)));

  uint32_t deviceExtensionNamesSize = 0;
  XR_CHECK(
      pfnGetVulkanDeviceExtensionsKHR(instance, systemId, 0, &deviceExtensionNamesSize, nullptr));
  std::vector<char> deviceExtensionNames(deviceExtensionNamesSize);
  XR_CHECK(pfnGetVulkanDeviceExtensionsKHR(instance, systemId, deviceExtensionNamesSize,
                                           &deviceExtensionNamesSize, &deviceExtensionNames[0]));
  std::vector<const char*> deviceExtensions = ParseExtensionString(&deviceExtensionNames[0]);

  VkPhysicalDeviceFeatures features{};

  VkDeviceCreateInfo deviceInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
  deviceInfo.flags = VkDeviceCreateFlags(0);
  deviceInfo.queueCreateInfoCount = 1;
  deviceInfo.pQueueCreateInfos = &queueInfo;
  deviceInfo.enabledLayerCount = 0;
  deviceInfo.ppEnabledLayerNames = nullptr;
  deviceInfo.enabledExtensionCount = (uint32_t)deviceExtensions.size();
  deviceInfo.ppEnabledExtensionNames = deviceExtensions.empty() ? nullptr : deviceExtensions.data();
  deviceInfo.pEnabledFeatures = &features;

  VK_CHECK(vkCreateDevice(m_vkPhysicalDevice, &deviceInfo, nullptr, &m_vkDevice));

  vkGetDeviceQueue(m_vkDevice, queueInfo.queueFamilyIndex, 0, &m_vkQueue);

  m_memAllocator.Init(m_vkPhysicalDevice, m_vkDevice);

  InitializeResources();

  m_graphicsBinding.instance = m_vkInstance;
  m_graphicsBinding.physicalDevice = m_vkPhysicalDevice;
  m_graphicsBinding.device = m_vkDevice;
  m_graphicsBinding.queueFamilyIndex = queueInfo.queueFamilyIndex;
  m_graphicsBinding.queueIndex = 0;

  return true;
}

void Example::InitializeResources() {
  VkSemaphoreCreateInfo semInfo{VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
  VK_CHECK(vkCreateSemaphore(m_vkDevice, &semInfo, nullptr, &m_vkDrawDone));

  if (!m_cmdBuffer.Init(m_vkDevice, m_queueFamilyIndex))
    XRC_THROW("Failed to create command buffer");
}

void Example::ShutdownDevice() {
  if (m_vkDevice != VK_NULL_HANDLE) {
    vkDeviceWaitIdle(m_vkDevice);

    for (auto& ctx : m_swapchainImageContextMap) {
      ctx.second->Reset();
    }
    m_swapchainImageContextMap.clear();

    m_queueFamilyIndex = 0;
    m_vkQueue = VK_NULL_HANDLE;
    if (m_vkDrawDone) {
      vkDestroySemaphore(m_vkDevice, m_vkDrawDone, nullptr);
      m_vkDrawDone = VK_NULL_HANDLE;
    }

    m_cmdBuffer.Reset();
    m_memAllocator.Reset();

    vkDestroyDevice(m_vkDevice, nullptr);
    m_vkDevice = VK_NULL_HANDLE;
  }
  if (m_vkInstance != VK_NULL_HANDLE) {
    vkDestroyInstance(m_vkInstance, nullptr);
    m_vkInstance = VK_NULL_HANDLE;
  }
}

const XrBaseInStructure* Example::GetGraphicsBinding() const {
  if (m_graphicsBinding.device) {
    return reinterpret_cast<const XrBaseInStructure*>(&m_graphicsBinding);
  }
  return nullptr;
}

int64_t Example::SelectColorSwapchainFormat(const int64_t* formatArray, size_t count) const {
  const std::array<VkFormat, 1> f{VK_FORMAT_B8G8R8A8_SRGB};

  const int64_t* formatArrayEnd = formatArray + count;
  auto it = std::find_first_of(formatArray, formatArrayEnd, f.begin(), f.end());

  if (it == formatArrayEnd) {
    assert(false);
    return formatArray[0];
  }

  return *it;
}

std::shared_ptr<SwapchainImageContext> Example::AllocateSwapchainImageStructs(
    size_t size, const XrSwapchainCreateInfo& swapchainCreateInfo) {
  auto derivedResult = std::make_shared<SwapchainImageContext>();

  std::vector<XrSwapchainImageBaseHeader*> bases =
      derivedResult->Create(m_vkDevice, &m_memAllocator, uint32_t(size), swapchainCreateInfo);

  for (auto& base : bases) {
    derivedResult->imagePtrVector.push_back(base);
    m_swapchainImageContextMap.emplace(std::make_pair(base, derivedResult));
  }

  std::shared_ptr<SwapchainImageContext> result =
      std::static_pointer_cast<SwapchainImageContext, SwapchainImageContext>(derivedResult);

  return result;
}

void Example::SetViewportAndScissor(const VkRect2D& rect) {
  VkViewport viewport{float(rect.offset.x),
                      float(rect.offset.y),
                      float(rect.extent.width),
                      float(rect.extent.height),
                      0.0f,
                      1.0f};
  vkCmdSetViewport(m_cmdBuffer.buf, 0, 1, &viewport);
  vkCmdSetScissor(m_cmdBuffer.buf, 0, 1, &rect);
}

void Example::ClearImageSlice(const XrSwapchainImageBaseHeader* colorSwapchainImage,
                              uint32_t imageArrayIndex) {
  auto swapchainContext = m_swapchainImageContextMap[colorSwapchainImage];
  uint32_t imageIndex = swapchainContext->ImageIndex(colorSwapchainImage);

  m_cmdBuffer.Clear();
  m_cmdBuffer.Begin();

  VkRect2D renderArea = {{0, 0}, {swapchainContext->size.width, swapchainContext->size.height}};
  SetViewportAndScissor(renderArea);

  VkRenderPassBeginInfo renderPassBeginInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
  swapchainContext->BindRenderTarget(imageIndex, imageArrayIndex, renderArea, &renderPassBeginInfo);
  vkCmdBeginRenderPass(m_cmdBuffer.buf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  static XrColor4f darkSlateGrey = {0.184313729f, 0.309803933f, 0.309803933f, 1.0f};
  static std::array<VkClearValue, 2> clearValues;
  clearValues[0].color.float32[0] = darkSlateGrey.r;
  clearValues[0].color.float32[1] = darkSlateGrey.g;
  clearValues[0].color.float32[2] = darkSlateGrey.b;
  clearValues[0].color.float32[3] = darkSlateGrey.a;
  clearValues[1].depthStencil.depth = 1.0f;
  clearValues[1].depthStencil.stencil = 0;
  std::array<VkClearAttachment, 2> clearAttachments{{
      {VK_IMAGE_ASPECT_COLOR_BIT, 0, clearValues[0]},
      {VK_IMAGE_ASPECT_DEPTH_BIT, 0, clearValues[1]},
  }};
  VkClearRect clearRect{renderArea, 0, 1};
  vkCmdClearAttachments(m_cmdBuffer.buf, 2, &clearAttachments[0], 1, &clearRect);

  vkCmdEndRenderPass(m_cmdBuffer.buf);

  m_cmdBuffer.End();
  m_cmdBuffer.Exec(m_vkQueue);
  m_cmdBuffer.Wait();
}

void Example::RenderView(const XrCompositionLayerProjectionView& layerView,
                         const XrSwapchainImageBaseHeader* colorSwapchainImage) {
  auto swapchainContext = m_swapchainImageContextMap[colorSwapchainImage];
  uint32_t imageIndex = swapchainContext->ImageIndex(colorSwapchainImage);

  m_cmdBuffer.Clear();
  m_cmdBuffer.Begin();

  const XrRect2Di& r = layerView.subImage.imageRect;
  VkRect2D renderArea = {{r.offset.x, r.offset.y},
                         {uint32_t(r.extent.width), uint32_t(r.extent.height)}};

  SetViewportAndScissor(renderArea);

  VkRenderPassBeginInfo renderPassBeginInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};

  swapchainContext->BindRenderTarget(imageIndex, layerView.subImage.imageArrayIndex, renderArea,
                                     &renderPassBeginInfo);

  vkCmdBeginRenderPass(m_cmdBuffer.buf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  vkCmdEndRenderPass(m_cmdBuffer.buf);

  m_cmdBuffer.End();
  m_cmdBuffer.Exec(m_vkQueue);
  m_cmdBuffer.Wait();
}

static XrResult CreateBasicInstance(XrInstance* instance) {
  XrInstanceCreateInfo createInfo{XR_TYPE_INSTANCE_CREATE_INFO};
  createInfo.applicationInfo.applicationVersion = 1;
  strcpy(createInfo.applicationInfo.applicationName, "conformance test");
  createInfo.applicationInfo.apiVersion = XR_CURRENT_API_VERSION;

  const char* extensions[] = {"XR_KHR_vulkan_enable"};
  createInfo.enabledExtensionCount = 1;
  createInfo.enabledExtensionNames = extensions;

  XrResult result = xrCreateInstance(&createInfo, instance);
  if (XR_FAILED(result)) {
    *instance = XR_NULL_HANDLE;
  }

  return result;
}

void Example::InitXR() {
  m_primaryViewType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

  XR_CHECK(CreateBasicInstance(&m_instance));

  XR_CHECK(CreateBasicSession(m_instance, &m_systemId, &m_session));

  XR_CHECK(xrEnumerateViewConfigurationViews(m_instance, m_systemId, m_primaryViewType, 0,
                                             &m_projectionViewCount, nullptr));

  std::vector<int64_t> swapchainFormats;
  {
    uint32_t countOutput;
    XR_CHECK(xrEnumerateSwapchainFormats(m_session, 0, &countOutput, nullptr));
    if (countOutput != 0) {
      swapchainFormats.resize(countOutput);
      XR_CHECK(xrEnumerateSwapchainFormats(m_session, (uint32_t)swapchainFormats.size(),
                                           &countOutput, swapchainFormats.data()));
    }
  }

  m_defaultColorFormat =
      SelectColorSwapchainFormat(swapchainFormats.data(), swapchainFormats.size());

  BeginSession();

  InitProjectionLayerHelper();

  Loop();
}

void Example::KillXR() {
  for (XrSpace space : m_spaces) {
    XR_CHECK(xrDestroySpace(space));
  }

  for (auto swapchain : m_createdSwapchains) {
    XR_CHECK(xrDestroySwapchain(swapchain.first));
  }

  xrDestroySession(m_session);

  ShutdownDevice();

  xrDestroyInstance(m_instance);
}

XrResult Example::CreateBasicSession(XrInstance instance, XrSystemId* systemId, XrSession* session,
                                     bool enableGraphicsSystem) {
  XrResult result;

  XrSystemGetInfo systemGetInfo{XR_TYPE_SYSTEM_GET_INFO, nullptr,
                                XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY};
  result = xrGetSystem(instance, &systemGetInfo, systemId);

  if (XR_SUCCEEDED(result)) {
    const XrBaseInStructure* graphicsBinding = nullptr;

    if (enableGraphicsSystem) {
      if (!InitializeDevice(instance, *systemId)) {
        return XR_ERROR_RUNTIME_FAILURE;
      }

      graphicsBinding = GetGraphicsBinding();
      assert(graphicsBinding);
    } else {
      assert(false);
      return XR_ERROR_RUNTIME_FAILURE;
    }

    XrSessionCreateInfo sessionCreateInfo{XR_TYPE_SESSION_CREATE_INFO, graphicsBinding, 0,
                                          *systemId};
    result = xrCreateSession(instance, &sessionCreateInfo, session);
  }

  return result;
}

bool Example::IterateFrame() {
  static int frames = 0;

  XrFrameState frameState{XR_TYPE_FRAME_STATE};
  XrFrameWaitInfo waitInfo{XR_TYPE_FRAME_WAIT_INFO};
  XR_CHECK(xrWaitFrame(m_session, &waitInfo, &frameState));

  XrFrameBeginInfo beginInfo{XR_TYPE_FRAME_BEGIN_INFO};
  XR_CHECK(xrBeginFrame(m_session, &beginInfo));

  UpdateProjectionLayer(frameState);
  EndFrame(frameState.predictedDisplayTime);

  frames++;

  if (frames > 100) return false;

  return true;
}

void Example::Loop() {
  ([&]() {
    while (IterateFrame()) {
    }
  }());
}

std::vector<XrViewConfigurationView> Example::EnumerateConfigurationViews() {
  std::vector<XrViewConfigurationView> views;

  uint32_t countOutput;
  XR_CHECK(xrEnumerateViewConfigurationViews(m_instance, m_systemId, m_primaryViewType, 0,
                                             &countOutput, nullptr));
  if (countOutput != 0) {
    views.resize(countOutput, {XR_TYPE_VIEW_CONFIGURATION_VIEW});
    XR_CHECK(xrEnumerateViewConfigurationViews(m_instance, m_systemId, m_primaryViewType,
                                               (uint32_t)views.size(), &countOutput, views.data()));
  }

  return views;
}

void Example::BeginSession() {
  XrSessionBeginInfo beginInfo{XR_TYPE_SESSION_BEGIN_INFO};
  beginInfo.primaryViewConfigurationType = m_primaryViewType;
  XR_CHECK(xrBeginSession(m_session, &beginInfo));
}

std::tuple<XrViewState, std::vector<XrView>> Example::LocateViews(XrSpace space,
                                                                  int64_t displayTime) {
  XrViewLocateInfo viewLocateInfo{XR_TYPE_VIEW_LOCATE_INFO};
  viewLocateInfo.displayTime = displayTime;
  viewLocateInfo.space = space;
  viewLocateInfo.viewConfigurationType = m_primaryViewType;
  XrViewState viewState{XR_TYPE_VIEW_STATE};
  std::vector<XrView> views(m_projectionViewCount, {XR_TYPE_VIEW});
  uint32_t viewCount = m_projectionViewCount;
  XR_CHECK(
      xrLocateViews(m_session, &viewLocateInfo, &viewState, viewCount, &viewCount, views.data()));

  return std::make_tuple(viewState, std::move(views));
}

void Example::EndFrame(XrTime predictedDisplayTime) {
  std::vector<XrCompositionLayerBaseHeader*> layers{
      reinterpret_cast<XrCompositionLayerBaseHeader*>(m_projLayer)};

  XrFrameEndInfo frameEndInfo{XR_TYPE_FRAME_END_INFO};
  frameEndInfo.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
  frameEndInfo.displayTime = predictedDisplayTime;
  frameEndInfo.layerCount = (uint32_t)layers.size();
  frameEndInfo.layers = layers.data();
  XR_CHECK(xrEndFrame(m_session, &frameEndInfo));
}

void Example::AcquireWaitReleaseImage(
    XrSwapchain swapchain,
    std::function<void(const XrSwapchainImageBaseHeader*)> doUpdate) {
  uint32_t imageIndex;
  XrSwapchainImageAcquireInfo acquireInfo{XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO};
  XR_CHECK(xrAcquireSwapchainImage(swapchain, &acquireInfo, &imageIndex));

  XrSwapchainImageWaitInfo waitInfo{XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO};
  XR_CHECK(xrWaitSwapchainImage(swapchain, &waitInfo));

  const XrSwapchainImageBaseHeader* image =
      m_swapchainImages[swapchain]->imagePtrVector[imageIndex];
  doUpdate(image);

  XrSwapchainImageReleaseInfo releaseInfo{XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO};
  XR_CHECK(xrReleaseSwapchainImage(swapchain, &releaseInfo));
}

XrSpace Example::CreateReferenceSpace(XrReferenceSpaceType type) {
  XrSpace space;
  XrReferenceSpaceCreateInfo createInfo{XR_TYPE_REFERENCE_SPACE_CREATE_INFO};
  createInfo.poseInReferenceSpace = XrPosef{{0, 0, 0, 1}, {0, 0, 0}};
  createInfo.referenceSpaceType = type;
  XR_CHECK(xrCreateReferenceSpace(m_session, &createInfo, &space));

  m_spaces.push_back(space);
  return space;
}

XrSwapchainCreateInfo Example::DefaultColorSwapchainCreateInfo(
    uint32_t width, uint32_t height, XrSwapchainCreateFlags createFlags /*= 0*/,
    int64_t format /*= -1*/) {
  if (format == -1) {  // Is -1 a safe "uninitialized" value?
    format = m_defaultColorFormat;
  }

  XrSwapchainCreateInfo createInfo{XR_TYPE_SWAPCHAIN_CREATE_INFO};
  createInfo.arraySize = 1;
  createInfo.format = format;
  createInfo.width = width;
  createInfo.height = height;
  createInfo.mipCount = 1;
  createInfo.faceCount = 1;
  createInfo.sampleCount = 1;
  createInfo.usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
  createInfo.createFlags = createFlags;
  return createInfo;
}

XrSwapchain Example::CreateSwapchain(const XrSwapchainCreateInfo& createInfo) {
  XrSwapchain swapchain;
  XR_CHECK(xrCreateSwapchain(m_session, &createInfo, &swapchain));

  // Cache the swapchain create info and image structs.
  m_createdSwapchains.insert({swapchain, createInfo});

  // Cache the swapchain image structs.
  uint32_t imageCount;
  XR_CHECK(xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr));
  std::shared_ptr<SwapchainImageContext> swapchainImages =
      AllocateSwapchainImageStructs(imageCount, createInfo);
  XR_CHECK(xrEnumerateSwapchainImages(swapchain, imageCount, &imageCount,
                                      swapchainImages->imagePtrVector[0]));
  m_swapchainImages[swapchain] = swapchainImages;

  return swapchain;
}

XrSwapchainSubImage Example::MakeDefaultSubImage(XrSwapchain swapchain,
                                                 uint32_t imageArrayIndex /*= 0*/) {
  // Look up the swapchain creation details to get default width/height.
  auto swapchainInfoIt = m_createdSwapchains.find(swapchain);
  assert(swapchainInfoIt != m_createdSwapchains.end());

  XrSwapchainSubImage subImage;
  subImage.swapchain = swapchain;
  subImage.imageRect = {
      {0, 0}, {(int32_t)swapchainInfoIt->second.width, (int32_t)swapchainInfoIt->second.height}};
  subImage.imageArrayIndex = imageArrayIndex;
  return subImage;
}

XrCompositionLayerProjection* Example::CreateProjectionLayer(XrSpace space) {
  // Allocate projection views and store.
  assert(m_projectionViewCount > 0);
  XrCompositionLayerProjectionView init{XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW};
  // Make sure the pose is valid
  init.pose.orientation.w = 1.0f;
  std::vector<XrCompositionLayerProjectionView> projViews(m_projectionViewCount, init);
  m_projectionViews.push_back(std::move(projViews));

  // Allocate projection and store.
  XrCompositionLayerProjection projection{XR_TYPE_COMPOSITION_LAYER_PROJECTION};
  projection.space = space;
  projection.viewCount = (uint32_t)m_projectionViews.back().size();
  projection.views = m_projectionViews.back().data();
  m_projections.push_back(projection);

  return &m_projections.back();
}

void Example::InitProjectionLayerHelper() {
  m_localSpace = CreateReferenceSpace(XR_REFERENCE_SPACE_TYPE_LOCAL);

  const std::vector<XrViewConfigurationView> viewProperties = EnumerateConfigurationViews();

  m_projLayer = CreateProjectionLayer(m_localSpace);
  for (uint32_t j = 0; j < m_projLayer->viewCount; j++) {
    const XrSwapchain swapchain = CreateSwapchain(DefaultColorSwapchainCreateInfo(
        viewProperties[j].recommendedImageRectWidth, viewProperties[j].recommendedImageRectHeight));
    const_cast<XrSwapchainSubImage&>(m_projLayer->views[j].subImage) =
        MakeDefaultSubImage(swapchain, 0);
    m_swapchains.push_back(swapchain);
  }
}

void Example::UpdateProjectionLayer(const XrFrameState& frameState) {
  auto viewData = LocateViews(m_localSpace, frameState.predictedDisplayTime);
  const auto& viewState = std::get<XrViewState>(viewData);

  std::vector<XrCompositionLayerBaseHeader*> layers;
  if (viewState.viewStateFlags & XR_VIEW_STATE_POSITION_VALID_BIT &&
      viewState.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) {
    const auto& views = std::get<std::vector<XrView>>(viewData);

    // Render into each view swapchain using the recommended view fov and pose.
    for (size_t view = 0; view < views.size(); view++) {
      AcquireWaitReleaseImage(
          m_swapchains[view],
          [&](const XrSwapchainImageBaseHeader* swapchainImage) {
            ClearImageSlice(swapchainImage, 0);

            const_cast<XrFovf&>(m_projLayer->views[view].fov) = views[view].fov;
            const_cast<XrPosef&>(m_projLayer->views[view].pose) = views[view].pose;
            RenderView(m_projLayer->views[view], swapchainImage);
          });
    }
  }
}

int main() {
  Example example;
  example.InitXR();
  return 0;
}
